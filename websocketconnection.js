var websocket;

window.onbeforeunload = function() 
{
	websocket.send("close connection");
};

$().ready(function()
{
	websocket = new WebsocketConnection();
	websocket.connect();
});

function getWebsocket()
{
	return websocket;
};

function WebsocketConnection()
{
	this.handler = new Array();
	this.socket;
	this.connectionRetrys = 0;
};

WebsocketConnection.prototype.connect = function()
{
	if(this.connectionRetrys < 5)
	{
		$('#websocketStatus').html("Websocket connecting. Failed attempts: "+this.connectionRetrys);
		var me = this;
		this.socket = new WebSocket("ws://" + HOST + ":2700/"); 
		this.socket.onopen = function(evt) {  me.onOpen(evt); };
		this.socket.onclose = function(evt) { me.onClose(evt); };
		this.socket.onmessage = function(evt) { me.onMessage(evt); };
		this.socket.onerror = function(evt) { me.onError; };
	}
	else
	{
		$('#websocketStatus').html("Websocket disconnected");
	}
};

WebsocketConnection.prototype.addHandler = function(command, func)
{
	if(this.handler == null) this.handler = new Array();
	this.handler[command] = func;
};

WebsocketConnection.prototype.onOpen = function(evt) 
{ 
	this.connectionRetrys = 0;
	console.log("WEBSOCKET CONNECTED");
	var us = getCookie("username");
	var pw = getCookie("password");
	if(us != null && pw != null)
	{
		saveLogin(us, pw);
		websocket.send("login "+us+" "+pw);
	}
	
	$('#websocketStatus').html("Websocket connected");
}; 

WebsocketConnection.prototype.onClose = function(evt) 
{ 
	this.connectionRetrys++;
	connect();
	console.log("WEBSOCKET DISCONNECTED"); 
};

WebsocketConnection.prototype.onMessage = function(evt) 
{ 
	if(evt.data == "clear")
	{
		$('#console').html("");
	}
	else
	{
		console.log("Get:"+evt.data); 
		$('#console').append(evt.data);
		var objDiv = document.getElementById("console");
		objDiv.scrollTop = objDiv.scrollHeight;
	}
};

WebsocketConnection.prototype.onError = function(evt) 
{ 
	connect();
	console.log("WEBSOCKET ERROR:"); 
	console.log(evt); 
};

WebsocketConnection.prototype.send = function(command)
{
	console.log("Send:"+command); 
	this.socket.send(command);
};