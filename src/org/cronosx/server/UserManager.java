package org.cronosx.server;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author prior
 * Holds all user-instances
 */
public class UserManager
{
	/**
	 * Main server instance
	 */
	private ServerGTA server;
	/**
	 * All registered user-instances
	 */
	private HashMap<String, User> users;
	private ArrayList<String> usernames;
	public UserManager(ServerGTA server)
	{
		users = new HashMap<String, User>();
		usernames = new ArrayList<String>();
		this.server = server;
		loadUsernames();
	}
	
	/**
	 * returns a TEMPORARY Array of all users registered on this server
	 * !DO NOT USE IF YOU DON'T KNOW WHAT YOU ARE DOING!
	 * As the returned users are NOT registered to the usermanager,
	 * they can NOT be COMPARED by the == OPERATOR
	 * This causes a MASSIVE LOAD on the database
	 * <p>
	 * @return an array of all Users
	 */
	public User[] getTemporaryArrayOfAllUsers()
	{
		User[] user = new User[usernames.size()];
		for(int i = 0; i < usernames.size(); i++)
		{
			String username = usernames.get(i);
			if(users.containsKey(username))
				user[i] = getUser(username);
			else
				user[i] = new User(username, server);
		}
		return user;
	}
	
	/**
	 * Saves all users, exports them to the database
	 */
	public void saveAllUsers()
	{
		for(String user:users.keySet())
			users.get(user).exportToDB();
	}

	/**
	 * Looks the username up at the already registered instances and if there is no instance, creates a new one and registers it
	 * <p>
	 * @param user username to look up
	 * @return a instance of the user
	 */
	public User getUser(String user)
	{
		if(users.containsKey(user))
			return users.get(user);
		else
		{
			User u = new User(user, server);
			users.put(user, u);
			return u; 
		}
	}
	
	/**
	 * Unregisters a user because its instance will no longer be needed
	 * <p>
	 * @param username user to unregister
	 */
	public void unregister(String username)
	{
		users.remove(username);
	}
	
	/**
	 * Creates a new line in the database for the user
	 * (Creates a new user)
	 * <p>
	 * @param username username to register
	 * @param password password for the user
	 */
	public void registerUser(String username, String password)
	{
		usernames.add(username);
		PreparedStatement stmt = null;
		try
		{
			stmt = server.getDatabaseConnection().getPreparedStatement("INSERT INTO Users(Username,Password,Permissions) VALUES (?, ?, 1)");
			stmt.setString(1, username);
			stmt.setString(2, new String(server.getSHA1(password)));
			stmt.executeUpdate();
			stmt = server.getDatabaseConnection().getPreparedStatement("INSERT INTO UserAttributes(`Username`,`Key`,`Value`) VALUES (?, 'Max-HP','100')");
			stmt.setString(1, username);
			stmt.executeUpdate();
			stmt = server.getDatabaseConnection().getPreparedStatement("INSERT INTO UserAttributes(`Username`,`Key`,`Value`) VALUES (?, 'HP','80')");
			stmt.setString(1, username);
			stmt.executeUpdate();
			stmt = server.getDatabaseConnection().getPreparedStatement("INSERT INTO UserAttributes(`Username`,`Key`,`Value`) VALUES (?, 'Money','20')");
			stmt.setString(1, username);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			server.getLog().error("Unabled to register user: "+e.getMessage());
		}
		finally
		{
			try
			{
				stmt.close();
			}
			catch(Exception e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			
		}	
	}
	
	/**
	 * Returns a list of all usernames registered to this Server
	 * <p>
	 * @return list of usernames
	 */
	public String[] getUsernames()
	{
		return usernames.toArray(new String[]{});
	}
	
	/**
	 * Returns whether username and password match in the database
	 * <p> 
	 * @param user username to test for
	 * @param password entered password
	 * @return whether password and username match
	 */
	public boolean isLoginCorrect(String user, String password)
	{
		PreparedStatement stmt= null;
		ResultSet rs=null;
		try
		{
			stmt = server.getDatabaseConnection().getPreparedStatement("SELECT Password FROM Users WHERE Username = ?");
			stmt.setString(1, user);
			stmt.execute();
			rs = stmt.getResultSet();
			if(rs.next())
			{
				String compare = rs.getString(1);
				return compare.equals(server.getSHA1(password));
			}
			else return false;
		}
		catch(Exception e)
		{
			server.getLog().error("Unabled to test for correct login: "+e.getMessage());
			return false;
		}
		finally
		{
			try
			{
				stmt.close();
				rs.close();
			}
			catch(Exception e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			
		}
	}
	
	public void loadUsernames()
	{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			stmt = server.getDatabaseConnection().getPreparedStatement("SELECT Username FROM Users");
			rs = stmt.executeQuery();
			while(rs.next())
			{
				usernames.add(rs.getString(1));
			}
		}
		catch(Exception e)
		{
			server.getLog().error("Unabled to get list of usernames: "+e.getMessage());
		}
		finally
		{
			try
			{
				stmt.close();
				rs.close();
			}
			catch(Exception e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			
		}
	}
	
	/**
	 * Use it to test whether a username is available or not
	 * <p>
	 * @param user username to test
	 * @return whether this username is not already taken
	 */
	public boolean isUsernameAvailable(String user)
	{
		return !usernames.contains(user);
	}
}
