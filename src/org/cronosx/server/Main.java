package org.cronosx.server;

public class Main
{
	public static void main(String[] args)
	{
		final Server server = new ServerGTA();
		server.start();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run()
			{
				server.getLog().log("Received CTRL+C, saving all remaining data");
				server.save();
			}
		});
	}
	
	/**
	 * Returns a string in the form xxx,xx% depending on the given number
	 * <p>
	 * @param f float to convert
	 * @return the value as percent
	 */
	public static String toPercent(float f)
	{
		return roundTo(f*100,2)+"%";
	}
	
	/**
	 * Returns true or false randomly, depending on the given proximity
	 * <p>
	 * @param prox proximity of the command to succeed
	 * @return success or failure
	 */
	public static boolean randomize(float prox)
	{
		return Math.random() <= prox;
	} 
	
	/**
	 * returns one random object of the given strings 
	 * <p>
	 * @param input strings to choose from
	 * @return on random string
	 */
	public static String oneOf(String... input)
	{
		int index = (int) (Math.random() * input.length);
		return input[index];
	}

	/**
	 * Rounds a number to a given number of decimal places
	 * <p>
	 * @param d number to round
	 * @param s number of decimal places
	 * @return rounded number
	 */
	public static double roundTo(double d, int s)
	{
		return Math.round((d * Math.pow(10, s))) / Math.pow(10, s);
	}
	
	public static int randomInRange(int lower, int upper)
	{
		return (int)(Math.random() * (upper - lower) + lower);
	}
}
