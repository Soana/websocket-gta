package org.cronosx.server;

import java.sql.SQLException;
import java.sql.Statement;

import org.cronosx.cgi.CGI;
import org.cronosx.gta.zones.ZoneArena;
import org.cronosx.webserver.Webserver;

/**
 * @author prior
 * Main Server instance
 */
public class ServerGTA extends Server
{
	private UserManager userManager;
	private ZoneManager zoneManager;
	
	public ServerGTA()
	{
		super();
		zoneManager = new ZoneManager(this);
		userManager = new UserManager(this);
		new Thread("Scheduled Saver Thread")
		{
			public void run()
			{
				try
				{
					Thread.sleep((long)getConfig().getInt("Saver-Interval", 1800));
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
				save();
			}
		}.start();
		((ZoneArena)zoneManager.getZone(ZoneArena.class)).loadHighscore();
	}
	
	public ZoneManager getZoneManager()
	{
		return zoneManager;
	}
	
	@Override
	public void shutDown()
	{
		//TODO: ADD FUNCTIONALITY
	}
	
	@Override
	public void save()
	{
		userManager.saveAllUsers();
		((ZoneArena)zoneManager.getZone(ZoneArena.class)).exportToDB();
	}
	
	/**
	 * @return the main user-manager holding all user-instances
	 */
	public UserManager getUserManager()
	{
		return userManager;
	}

	@Override
	public DefaultWebSocketListener getDefaultWebSocketListener()
	{
		return new DefaultWebSocketListenerGTA(this);
	}

	@Override
	public Webserver getDefaultWebserver()
	{
		return new Webserver(getLog(), getConfig(), this);
	}

	@Override
	protected CGI getDefaultCGIHandler()
	{
		return null;
	}

	@Override
	public void createDatabase(Statement stmt)
	{

		try
		{
			getLog().log("Creating database.");
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + getConfig().getStr("dbName", "jiso"));
			getLog().log("Creating table \"Users\".");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Users (" +
					"ID 					INT				NOT NULL AUTO_INCREMENT PRIMARY KEY,	" +
					"Username				VARCHAR	( 16),											" +
					"Password				VARCHAR	( 40)," +
					"Permissions			INT		(  1))");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS UserAttributes (" +
					"ID 					INT				NOT NULL AUTO_INCREMENT PRIMARY KEY,	" +
					"Username				VARCHAR	( 16),											" +
					"`Key`					VARCHAR	( 64)," +
					"`Value`				TEXT)");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Items (" +
					"ID 					INT				NOT NULL AUTO_INCREMENT PRIMARY KEY,	" +
					"Username				VARCHAR	( 16),											" +
					"Type					VARCHAR	( 64)," +
					"Amount					TEXT)");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS ItemAttributes (" +
					"ID 					INT				NOT NULL AUTO_INCREMENT PRIMARY KEY,	" +
					"Item					INT,													" +
					"`Key`					VARCHAR	( 64)," +
					"`Value`				TEXT)");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Arena (" +
					"ID 					INT				NOT NULL AUTO_INCREMENT PRIMARY KEY,	" +
					"Player1				VARCHAR ( 16),													" +
					"Player2				VARCHAR	( 16)," +
					"Done					INT ( 1)," +
					"Money					INT)");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS ArenaBets (" +
					"ID 					INT				NOT NULL AUTO_INCREMENT PRIMARY KEY,	" +
					"Fight					INT,											" +
					"Username				VARCHAR	( 16)," +
					"BetOn					VARCHAR	( 16)," +
					"Amount					FLOAT)");
			getLog().log("Done.");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
