package org.cronosx.server;

import org.cronosx.websockets.WebSocket;

/**
 * @author prior
 * This Websocketlistener will guard all websockets whose users have not yet logged in
 */
public class DefaultWebSocketListenerGTA extends DefaultWebSocketListener
{
	private ServerGTA server;
	
	/**
	 * @param server the main server instance
	 */
	public DefaultWebSocketListenerGTA(ServerGTA server)
	{
		super(server);
		this.server = server;
	}


	@Override
	public void onHandshakeSuccessfull(WebSocket origin)
	{
		origin.send("Hallo!<br/>" +
				"Du kannst dich per \"login\" einloggen,<br/>" +
				"per \"registrieren\" registrieren oder<br/>" +
				"mit dem Befehl \"login-speichern\" deinen Login speichern, sodass du zukünftig automatisch angemeldet wirst<br/>");
	}
	
	@Override
	protected void parseMessage(String s, WebSocket origin)
	{
		server.getLog().log("Executing Websocket-command: \"" + s + "\"", 95);
		String[] param = s.split(" ");
				if(param[0].equals("login")) 					login(param, origin);
		else 	if(param[0].equals("registrieren"))					register(param, origin);
		else 	if(param[0].equals("close"))					close(origin);
		else 	unknownCommand(origin);
	}
	
	/**
	 * Will be called when the user enters a unknown command
	 * <p>
	 * @param origin websocket the command came from 
	 */
	private void unknownCommand(WebSocket origin)
	{
		origin.send("Komischer Befehl....\n");
	}
	
	/**
	 * closes a websocket-connection
	 * <p>
	 * @param origin websocket that will be closed
	 */
	private void close(WebSocket origin)
	{
		origin.close();
	}
	
	/**
	 * Will be called when the user enters "register ~", checks the command for correctness and registers the user if possible
	 * <p>
	 * @param params parameters the user entered
	 * @param origin websocket the command came from
	 */
	private void register(String[] params, WebSocket origin)
	{
		if(params.length >= 3 && params[1].length() >= 4 && params[1].length() <= 16 && server.getUserManager().isUsernameAvailable(params[1])
		&& params[2].length() >= 4)
		{
			server.getUserManager().registerUser(params[1], params[2]);
			origin.send("Erflogreich registriert! Jetzt musst du dich noch einloggen....");
		}
		else
		{
			origin.send("Entweder hast du dich vertippt, oder der Benutzername ist schon weg :( ... <br/>\"register [benutzername (mind. 4 zeichen/max. 16)] [passwort(mind. 4 zeichen)]\"<br/>");
		}
	}
	
	/**
	 * Will be called the the user enters "login ~", checks the command for correctness and loggs the user in if possible.
	 * If the user was logged in, the websocketlistener will be set to the users instance
	 * <p>
	 * @param params params the user entered
	 * @param origin websocket the command came from
	 */
	private void login(String[] params, WebSocket origin)
	{
		if(params.length >= 3 && server.getUserManager().isLoginCorrect(params[1], params[2]))
		{
			origin.setWebSocketListener(server.getUserManager().getUser(params[1]));
			server.getUserManager().getUser(params[1]).registerWebsocket(origin);
		}
		else
		{
			origin.send("Falscher login. \n login [benutzername] [passwort]");
		}
	}
}
