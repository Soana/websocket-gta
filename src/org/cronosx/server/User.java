package org.cronosx.server;

import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.cronosx.gta.Query;

import org.cronosx.gta.Fight;
import org.cronosx.gta.Fighting;
import org.cronosx.gta.Item;
import org.cronosx.gta.Person;
import org.cronosx.gta.Quest;
import org.cronosx.gta.Zone;
import org.cronosx.gta.querys.QueryAttack;
import org.cronosx.gta.querys.QueryConversation;
import org.cronosx.gta.querys.QueryDefend;
import org.cronosx.gta.querys.QueryDriveAround;
import org.cronosx.gta.querys.QueryEssen;
import org.cronosx.gta.querys.QueryHerausforderung;
import org.cronosx.gta.querys.QuerySteal;
import org.cronosx.gta.querys.QueryWalkAround;
import org.cronosx.gta.zones.ZoneGhetto;
import org.cronosx.websockets.WebSocket;
import org.cronosx.websockets.WebSocketListener;

/**
 * @author prior
 * Represents a user
 * optionally holding a websocket, receiving commands from the client
 */
public class User implements WebSocketListener, Fighting
{
	private WebSocket websocket;
	private ServerGTA server;
	private String userName;
	private String password;
	private float money;
	private int hp;
	private int maxHp;
	private boolean closed;
	private HashMap<String, String> keys;
	private HashMap<String, Method> globalCommands;
	private Class<? extends Zone> zone;
	private ArrayList<Item> inventory;
	private Query query;
	private Fight fightIn;
	private ArrayList<Quest> quests;
	
	@SuppressWarnings("unchecked")
	public User(String userName, ServerGTA server)
	{
		keys = new HashMap<String, String>();
		this.userName = userName;
		this.server = server;
		inventory = new ArrayList<Item>();
		importFromDB();
		try
		{
			zone = (Class<? extends Zone>) Class.forName(getKeyAsString("Zone"));
		}
		catch(Exception e)
		{
			zone = ZoneGhetto.class;
		}
		money = getKeyAsFloat("Money");
		hp = getKeyAsInt("HP");
		maxHp = getKeyAsInt("Max-HP");
		globalCommands = new HashMap<String, Method>();
		quests = new ArrayList<Quest>();
		registerCommand("?", "help");
		registerCommand("Wo bin ich?", "wo");
		registerCommand("Fahren", "startDriveAround");
		registerCommand("Klauen", "startSteal");
		registerCommand("Umsehen", "lookAround");
		registerCommand("Laufen", "startWalkAround");
		registerCommand("Sagen", "chat");
		registerCommand("Inventar", "inventoryDisplay");
		registerCommand("Herausfordern", "forderHeraus");
		registerCommand("Ansprechen", "sprechAn");
		registerCommand("Essen", "essen");
		registerCommand("Gesundheit", "gesundheit");
		registerCommand("Speichern", "save");
		getZone().enter(this);
		
	}
	/**
	 * Looks up a quest by it's class and returns, whether the user has this quest.
	 * This includes BOTH quests that are finished and such that are not
	 * <p>
	 * @see hasQuest(String tag)
	 * @param clazz quest to test on
	 * @return whether the player has this quest
	 */
	public boolean hasQuest(Class<? extends Quest> clazz)
	{
		for(Quest q: getQuests()) if(q.getClass().equals(clazz)) return true;
		return false;
	}
	
	/**
	 * Gives the player an amount of healthpoints and displays him a message
	 * <p>
	 * @param h healthpoints to give
	 */
	public void doHP(int h)
	{
		this.hp = ((h + this.hp > getMaxHP()) ? getMaxHP() : h + this.hp);
		send("Deine HP wurden um <hp>"+h+"</hp> Punkte wieder aufgefüllt! (<hp>"+getHP()+"</hp>/"+getMaxHP()+")\n");
	}
	
	/**
	 * @return an array of all quests the player is currently in
	 */
	public Quest[] getQuests()
	{
		return quests.toArray(new Quest[]{});
	}
	
	
	/**
	 * Gives the player a particular quest
	 * <p>
	 * @param q quest to give
	 */
	public void addQuest(Quest q)
	{
		send("<neutral>Quest hinzugefügt: </neutral>"+q.getName()+"\n");
		quests.add(q);
		setKey(q.getQuestTag(), "0");
	}
	
	/**
	 * Looks up a quest by it's quest-tag and returns, whether the user has this quest.
	 * This includes BOTH quests that are finished and such that are not
	 * <p>
	 * @see isInQuest(String tag)
	 * @see hasQuest(Class<? extends Quest> clazz)
	 * @param tag tag to test on
	 * @return whether the player has this quest
	 */
	public boolean hasQuest(String tag)
	{
		return keys.keySet().contains(tag);
	}
	
	/**
	 * Looks up a quest by it's quest-tag and returns, whether the user has this quest
	 * This includes ONLY quests that are not yet finished 
	 * @param tag tag to test on
	 * @return whether the player is currently doing this quest
	 */
	public boolean isInQuest(String tag)
	{
		return keys.keySet().contains(tag)
				&& keys.get(tag) != "Finished";
	}
	
	/**
	 * Removes a quest from the users list of quests
	 * <p>
	 * @param q quest to remove
	 */
	public void removeQuest(Quest q)
	{
		quests.remove(q);
	}
	/**
	 * @return the amount of healthpoints this user currently has
	 */
	public int getHP()
	{
		return hp;
	}
	
	/**
	 * Removes a given amount of a given type of items from the users ineventory,
	 * displays him a message and removes the whole instance if the lasting amount is less or equal zero
	 * <p>
	 * @param type type of item to remove
	 * @param amount amount to remove
	 */
	public void removeFromInventory(String type, int amount)
	{
		send("<bad>"+amount+"x</bad> "+type+" aus dem Inventar entfernt\n");
		Item it = this.getInventoryItem(type);
		if(it.getAmount() <= amount) inventory.remove(it);
		else it.doAmount(-amount);
	}
	
	/**
	 * Calls removeFromInventory(@param type, 1)
	 * Removes the amount of one item of the given type from the users inventory
	 * <p>
	 * @param type type to remove
	 * @see removeFromInventory(String type, int amount)
	 */
	public void removeFromInventory(String type)
	{
		removeFromInventory(type, 1);
	}
	
	/**
	 * @return the maximum amount of healthpoints this user can have
	 */
	public int getMaxHP()
	{
		return maxHp;
	}
	
	/**
	 * Registers a global command that can always be entered
	 * <p>
	 * These command can always be invoked, not depending on where the player is or what he is doing.
	 * The second parameter contains the name of the method to be invoked when the user entered the first parameter
	 * The method has to have exactly one parameter of the type String, the exact entered command will be passed to
	 * <p>
	 * @param s command to register to
	 * @param m method to be invoked when command is entered
	 */
	public void registerCommand(String s, String m)
	{
		try
		{
			globalCommands.put(s, getClass().getMethod(m, String.class));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Looks up an item in the users inventory and returns whether the user has this item in his inventory
	 * <p>
	 * @param name type to look up
	 * @return whether the user has this item in his inventory
	 */
	public boolean inventoryContainsItem(String name)
	{
		for(Item it:getInventory())
		{
			if(it.getName().toLowerCase().contains(name.toLowerCase())) return true;
		}
		return false;
	}
	
	/**
	 * Looks up the given type in the user inventory and returns the matching instance of Item.
	 * Return null when the user does not have this item
	 * <p>
	 * @param name type to look up
	 * @return instance of the item the user has
	 */
	public Item getInventoryItem(String name)
	{
		for(Item it:getInventory())
		{
			if(it.getName().contains(name)) return it;
		}
		return null;
	}
	
	/**
	 * Starts a new query for the user and puts the user into the query
	 * <p>
	 * @see Query
	 * @param q query to start
	 */
	public void startQuery(Query q)
	{
		this.query = q;
		q.onQueryOpen();
	}
	
	/**
	 * Immediately closes the current query and sets the user back to default interaction-mode
	 */
	public void closeQuery()
	{
		this.query = null;
	}
	
	/**
	 * @return whether the user is currently in a query
	 */
	public boolean isInQuery()
	{
		return query != null;
	}
	
	/**
	 * Moves the user to a zone
	 * <p>
	 * Removes the user from the old zone, makes him leave it, moves him to the new zone and makes him enter it
	 * <p>
	 * @param i zone to set
	 */
	public void setZone(Class<? extends Zone> i)
	{
		getZone().leave(this);
		zone = i;
		getZone().enter(this);
	}
	
	/**
	 * Removes the given item from this uers inventory
	 * <p>
	 * @param item item to remove
	 */
	public void removeFromInventory(Item item)
	{
		inventory.remove(item);
	}

	/**
	 * Sets the users attributes(key) to the given value
	 * <p>
	 * These values will be saved persistent in the database
	 * <p>
	 * @param key key to set
	 * @param value value to set
	 */
	public void setKey(String key, String value)
	{
		this.keys.put(key, value);
	}

	/**
	 * Looks the key up and returns the value as a String
	 * <p>
	 * @param key key to look up
	 * @return the value for the key as String
	 */
	public String getKeyAsString(String key)
	{
		return keys.get(key);
	}

	/**
	 * Looks the key up and return the value as an integer
	 * <p>
	 * @param key key to look up
	 * @return the value for the key parsed as Integer
	 * @throws NumberFormatException when the value for the key is not a valid integer
	 */
	public int getKeyAsInt(String key)
	{
		if(!keys.containsKey(key)) return 0;
		return Integer.parseInt(keys.get(key));
	}

	/**
	 * Looks the key up and return the value as a boolean
	 * <p>
	 * @param key key to look up
	 * @return the value for the key parsed as boolean
	 */
	public boolean getKeyAsBoolean(String key)
	{
		if(!keys.containsKey(key)) return false;
		return Boolean.parseBoolean(keys.get(key));
	}

	/**
	 * Looks the key up and return the value as a float
	 * <p>
	 * @param key key to look up
	 * @return the value for the key parsed as float
	 * @throws NumberFormatException when the value for the key is not a valid float
	 */
	public float getKeyAsFloat(String key)
	{
		if(!keys.containsKey(key)) return 0;
		try
		{
			return Float.parseFloat(keys.get(key));
		}
		catch(NumberFormatException e)
		{
			server.getLog().error("Bad Numberformat: \""+keys.get(key)+"\" for key \""+key+"\"");
			return 0;
		}
	}
	
	
	/**
	 * You should not use this, if not particular necessary
	 * <p>
	 * @see getKeyAsFloat
	 * @see getKeyAsInt
	 * @see getKeyAsString
	 * @see getKeyAsBoolean
	 * @see setKey
	 * @return the raw HashMap of the users attributes
	 */
	public HashMap<String, String> getKeys()
	{
		return keys;
	}
	
	/**
	 * Gives this player the item and merges it with existing items where possible
	 * <p>
	 * The items will only be merged when all attributes are the same
	 * <p>
	 * @param item item to give to this player
	 */
	public void giveItem(Item item)
	{
		send("<good>"+item.getAmount()+"x</good> "+item.getName()+" dem Inventar hinzugefügt\n");
		boolean merged = false;
		for(Item i2:inventory)
		{
			if(i2.getName().equals(item.getName()))
			{
				if(i2.getAttributes().isEmpty() && item.getAttributes().isEmpty())
				{
					merged = true;
					i2.doAmount(item.getAmount());
					break;
				}
				else
				{
					boolean wrong = false;
					for(String s: item.getAttributes().keySet())
					{
						if(i2.getAttributes().containsKey(s) &&
							i2.getAttributes().get(s).equals(item.getAttributes().get(2)))
						{
							wrong = true;
							break;
						}
					}
					if(!wrong)
					{
						merged = true;
						i2.doAmount(item.getAmount());
						break;
					}
				}
			}
		}
		if(!merged) inventory.add(item);
	}

	@Override
	public void onMessage(String s, WebSocket origin)
	{
		if(websocket == null || websocket != origin) websocket = origin;
		server.getLog().log("Running command \""+s+"\" for user \""+userName+"\"", 99);
		if(s.equals("close connection")) close();
		else parseMessage(s);
	}

	/**
	 * Registers a websocket to this user 
	 * <p>
	 * @param websocket websocket to register to this user
	 */
	public void registerWebsocket(WebSocket websocket)
	{
		this.websocket= websocket;
		websocket.send("clear"); 
		sendLine("Willkommen, " + getName());
		help("");
	}
	
	@Override
	public void onClose(WebSocket origin)
	{
		close();
	}
	
	/**
	 * Returns the Zone the user is currently in
	 * <p>
	 * @return zone the user is currently in
	 */
	public Zone getZone()
	{
		return server.getZoneManager().getZone(zone);
	}
	
	/**
	 * Sends a string to the websocket-endpoint and formats it correctly (colors, escaping, etc)
	 * <p>
	 * @param s string to send
	 */
	public void send(String s) 
	{ 
		if(websocket != null) websocket.send("<message>"+s
			.replace("\n", "<br />")
			.replace("<command>", "<a class=\"command" +
					"\" href=\"#\" onclick=\"javascript: $('#prompt').val($(this).text()); var e = jQuery.Event('keyup'); e.which = 13; $('#prompt').trigger(e);\">")
			.replace("</command>", "</a>")+"</message>");
	}
	
	
	/**
	 * Invoked when the user entered something
	 * <p>
	 * Parses a message and invokes the registered command, or passes it to the current query
	 * <p>
	 * @param s message to parse
	 */
	private void parseMessage(String s)
	{
		if(!isInQuery())
		{
			if(!zoneHandle(s))
				if(!questHandle(s))
					if(!globalHandle(s))
						chat("sagen "+s);
		}
		else 
		{
			if(s.toUpperCase().equals("ABBRECHEN")) 
			{
				if(query.canAbort())
				{
					closeQuery();
					send("ja, ich lass es wohl lieber bleiben...\n");
				}
				else
				{
					send("Scheisse, ich kanns einfach nicht lassen!\nIch kann jetzt nicht einfach aufhören!\n");
				}
			}
			else
			{
				try
				{
					if(query != null) query.getAction().invoke(query, s);
					else closeQuery();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Handles the message the user entered as a zone-internal command
	 * <p>
	 * Tests if the zone has registered the command the user entered and invokes it, if so
	 * <p>
	 * @param s string to handle
	 */
	private boolean zoneHandle(String s)
	{
		boolean found = false;
		String sl = s.toLowerCase();
		for(String s1:getZone().getCommands().keySet())
		{
			if(sl.length() >= s1.length() && s1.toLowerCase().equals(sl.substring(0,s1.length()))) try
			{
				getZone().getCommands().get(s1).invoke(getZone(), this, s);
				found = true;
			}
			catch(Exception e)
			{
				found = false;
				e.printStackTrace();
			}
		}
		return found;
	}
	
	/**
	 * Handles the command the user entered as a quest-internal command
	 * <p>
	 * @param s string to handle
	 * @return whether the command the user entered was a quest-internal command
	 */
	private boolean questHandle(String s)
	{
		boolean found = false;
		String sl = s.toLowerCase();
		for(Quest q:getQuests())
		{
			for(String s1:q.getCommands().keySet())
			{
				if(sl.length() >= s1.length() && s1.toLowerCase().equals(sl.substring(0,s1.length()))) try
				{
					q.getCommands().get(s1).invoke(q, s);
					found = true;
				}
				catch(Exception e)
				{
					found = false;
					e.printStackTrace();
				}
			}
		}
		return found;
	}
	
	/**
	 * Handles the message the user entered as a global command
	 * <p>
	 * Tests if the command the user entered is a global command and invokes it, if so
	 * <p>
	 * @param s string to handle
	 */
	private boolean globalHandle(String s)
	{
		boolean found = false;
		String sl = s.toLowerCase();
		for(String s1:globalCommands.keySet())
		{
			if(sl.length() >= s1.length() && s1.toLowerCase().equals(sl.substring(0,s1.length()))) try
			{
				globalCommands.get(s1).invoke(this, s);
				found = true;
			}
			catch(Exception e)
			{
				found = false;
				e.printStackTrace();
			}
		}
		return found;
	}
	
	
	//****************************************//
	//**          SOCKET-COMMANDS           **//
	//****************************************//
	
	/**
	 * Displays the user his current inventory and his amount of money
	 * <p>
	 * @param s command the user entered
	 */
	public void inventoryDisplay(String s)
	{
		String s2 = "Du trägst folgende Sachen bei dir:\n";
		for(Item i:inventory)
		{
			s2 += "<good>"+i.getAmount()+"x</good> <neutral>"+i.getName()+"</neutral> (ATK: <atk>" + i.getATK() + "</atk>) (DEF: <def>" + i.getDEF() + "</def>) (PRB: <neutral>" + Main.toPercent(i.getPRB()) + "</neutral>)\n";
		}
		send(s2.substring(0,s2.length()-2)+"\n"+"Du hast <money>"+Main.roundTo(getMoney(),2)+"</money> Euro\n");
	}

	/**
	 * Called when the user user the "Herausfordern" command.
	 * Starts a query to let the user choose a victim he can attack
	 * <p>
	 * @param s ignored
	 */
	public void forderHeraus(String s)
	{
		if(this.getZone().isSaveZone()) 
			send("Hier sollte ich vielleicht niemanden angreifen. Das könnte Schwierigkeiten geben!\n");
		else	
		{
			startQuery(new QueryHerausforderung(this));
		}
	}
	
	/**
	 * Saves the users information to the database and displays the user a message.
	 * Calle when the user used the "speichern" command
	 * <p>
	 * @param s ignored
	 */
	public void save(String s)
	{
		setKey("Zone", getZone().getClass().getName());
		send("Erfolgreich gespeichert!\n");
		exportToDB();
	}

	/**
	 * Starts a Conversation-Query when the user wants to talk to a NPC
	 * <p>
	 * @param s ignored
	 */
	public void sprechAn(String s)
	{
		startQuery(new QueryConversation(this));
	}

	/**
	 * Send the user a message containing information about his healthpoints
	 * <p>
	 * @param s ignored
	 */
	public void gesundheit(String s)
	{
		send("Du hast noch <hp>"+getHP()+"HP</hp> von maximal <good>"+getMaxHP()+"HP</good>\n");
	}

	/**
	 * Starts a Query that lets the user eat something from his inventory
	 * <p>
	 * @param s ignored
	 */
	public void essen(String s)
	{
		startQuery(new QueryEssen(this));
	}
	
	/**
	 * Shows the user, where he is
	 * <p>
	 * @param string command the user entered
	 */
	public void wo(String string)
	{
		send("Du bist "+getZone().getNamePrefixed()+"\n");
	}
	
	/**
	 * Sends the rest of the command the user entered as a chat-message to all other players that are currently in the same zone
	 * <p>
	 * @param string command the user entered
	 */
	public void chat(String string)
	{
		System.out.println(string);
		if(getZone() != null && string.length() > 6)
		{
			getZone().broadcast(userName + " sagt: " + string.substring(6)
					.replace("<", "&lt;")
					.replace(">", "&gt;")+"\n");
		}
	}
	
	/**
	 * Displays the player all commands he can currently do.
	 * Including both global and zone-internal commands
	 * <p>
	 * @param string command the user entered
	 */
	public void help(String string)
	{
		String s = "Du kannst folgendes machen: \n";
		for(String s2: globalCommands.keySet()) s += "<command>" + s2 + "</command>\n";
		for(String s2: getZone().getCommands().keySet()) s += "<command>" + s2 +"</command>\n";
		for(Quest q:getQuests())
		for(String s2: q.getCommands().keySet()) s += "<command>" + s2 +"</command>\n";
		sendLine(s.substring(0, s.length()-2) +"\n");
	}
	
	/**
	 * Tells the player which other, seeable players are currently in the same zone, if the zone is lookable,
	 * if not, tells the player it would be too dark to look around
	 * <p>
	 * @param string command the user entered
	 */
	public void lookAround(String string)
	{
		if(getZone().canLook(this))
		{
			String su = "Folgende Leute stehen hier so rum: \n    ";
			for(User u:getZone().getUsers())
			{
				su += u.getName() +", ";
			}
			for(Person p: getZone().getPersons()){
				su += p.getName() + ", ";
			}
			send(su.substring(0, su.length() -2)+"\n");	
		}
		else
			send("Dafür ist es hier zu dunkel...\n");
	}
	
	
	/**
	 * Starts the walking-query for the player
	 * <p>
	 * @param string command the user entered
	 */
	public void startWalkAround(String string)
	{
		startQuery(new QueryWalkAround(this));
	}
	
	/**
	 * Starts the stealing-query for the player
	 * <p>
	 * @param string command the user entered
	 */
	public void startSteal(String string)
	{
		if(getZone().canSteal(this))
			startQuery(new QuerySteal(this));
		else send("Ganz ehrlich? was soll ich deiner Meinung nach hier klauen?\n");
	}
	
	/**
	 * Starts the driving-query for the player
	 * <p>
	 * @param string command the user entered
	 */
	public void startDriveAround(String string)
	{
		if(getZone().canDrive())
			startQuery(new QueryDriveAround(this));
		else
			send("Wie soll ich HIER bitte mit einem Auto rumfahren!!?\n");
	}
	
	/**
	 * Not invoked by the player himself but by the client-javascript.
	 * called when the user closes the browser-window and the websocket-session ended
	 * <p>
	 * Exports the user and his inventory to the database
	 * <p>
	 * @param string command the user entered
	 */
	public void logout()
	{
		setKey("Zone", getZone().getClass().getName());
		getZone().leave(this);
		exportToDB();
		server.getUserManager().unregister(getName());
	}
	
	
	//****************************************//
	//**             GETTERS                **//
	//****************************************//

	
	/**
	 * Checks if the amount of money was available and if so, removes the amount from the users pocket
	 * <p>
	 * @param value value to check
	 * @return if the amount was available
	 */
	public boolean checkMoney(float value)
	{
		if(getMoney() >= value)
		{
			doMoney(-value);
			return true;
		}
		else return false;
	}
	
	/**
	 * @return whether the end-point websocket is closed
	 */
	private boolean isClosed()
	{
		return closed;
	}
	
	/**
	 * Closes the end-point websocket and invokes logout()
	 * Unregisters the user from the servers usermanager
	 */
	private void close()
	{
		if(!isClosed())
		{
			logout();
			if(websocket != null) websocket.close();
			server.getUserManager().unregister(this.getName());
			closed = true;
		}
	}
	/**
	 * @return the money the user currently has
	 */
	public float getMoney()
	{
		return money;
	}
	
	/**
	 * Sets the users money absolutely
	 * <p>
	 * @param i value to set
	 */
	public void setMoney(float i)
	{
		money = i;
	}
	
	/**
	 * Sets the users money relatively to his current amount of money
	 * <p>
	 * @param i value to increase by
	 */
	public void doMoney(float i)
	{
		setMoney(getMoney() + i);
	}
	
	/**
	 * Imports the user and all his attributes and items from the database
	 * <p>
	 * Please don't look at this code. It is dirty :(
	 */
	private void importFromDB()
	{
		PreparedStatement stmt= null;
		PreparedStatement stmt2= null;
		ResultSet rs=null;
		ResultSet rs2=null;
		try
		{
			//User
			
			stmt = server.getDatabaseConnection().getPreparedStatement("SELECT Password FROM Users WHERE Username = ?");
			stmt.setString(1, userName);
			stmt.execute();
			rs = stmt.getResultSet();
			if(rs.next())
			{
				password = rs.getString(1);
			}
			
			//Attributes
			
			try
			{
				stmt.close();
				rs.close();
			}
			catch(SQLException e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			stmt = server.getDatabaseConnection().getPreparedStatement("SELECT `Key`, `Value` FROM UserAttributes WHERE Username = ?");
			stmt.setString(1, userName);
			stmt.execute();
			rs = stmt.getResultSet();
			while(rs.next())
			{
				keys.put(rs.getString(1), rs.getString(2));
			}
			
			//Inventory
			
			try
			{
				stmt.close();
				rs.close();
			}
			catch(SQLException e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			stmt = server.getDatabaseConnection().getPreparedStatement("SELECT `ID`, `Type`, `Amount` FROM Items WHERE Username = ?");
			stmt.setString(1, userName);
			stmt.execute();
			rs = stmt.getResultSet();
			while(rs.next())
			{
				stmt2 = server.getDatabaseConnection().getPreparedStatement("SELECT `Key`, `Value` FROM ItemAttributes WHERE Item = ?");
				stmt2.setInt(1, rs.getInt(1));
				stmt2.execute();
				rs2 = stmt2.getResultSet();
				HashMap<String, String> attributes = new HashMap<String, String>();
				while(rs2.next())
				{
					attributes.put(rs2.getString(1), rs2.getString(2));
				}
				inventory.add(new Item(rs.getInt(1), rs.getString(2), rs.getInt(3), attributes, this));
			}
		}
		catch(SQLException e)
		{
			server.getLog().error("Unabled to grab userdata: "+e.getMessage());
		}
		finally
		{
			try
			{
				stmt.close();
				rs.close();
				stmt2.close();
				rs2.close();
			}
			catch(SQLException e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			catch(NullPointerException e)
			{
				
			}
			
		}
	}
	
	/**
	 * Exports the user and all his inventory and attributes to the database
	 * <p>
	 * Please don't look at this code. It is dirty :(
	 */
	public void exportToDB()
	{
		setKey("Money", getMoney()+"");
		setKey("HP", getHP()+"");
		setKey("Max-HP", getMaxHP()+"");
		server.getLog().log("Saving userinformation for user: \""+userName+"\"", 9);
		PreparedStatement stmt= null;
		try
		{
			//User 
			
			stmt = server.getDatabaseConnection().getPreparedStatement("UPDATE Users SET Password = ? WHERE Username = ?");
			stmt.setString(1, password);
			stmt.setString(2, userName);
			stmt.executeUpdate();
			try
			{
				stmt.close();
			}
			catch(SQLException e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			
			//UserAttributes
			
			stmt = server.getDatabaseConnection().getPreparedStatement("DELETE FROM UserAttributes WHERE Username = ?");
			stmt.setString(1, userName);
			stmt.executeUpdate();
			try
			{
				stmt.close();
			}
			catch(SQLException e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			for(String key : keys.keySet())
			{
				stmt = server.getDatabaseConnection().getPreparedStatement("INSERT INTO UserAttributes(`Key`, `Value`, Username) VALUES (?, ?, ?)");
				stmt.setString(1, key);
				stmt.setString(2, keys.get(key));
				stmt.setString(3, userName);
				stmt.executeUpdate();
				try
				{
					stmt.close();
				}
				catch(SQLException e)
				{
					server.getLog().error("Unabled to close Query: "+e.getMessage());
				}
			}
			
			//Inventory

			stmt = server.getDatabaseConnection().getPreparedStatement("DELETE FROM Items WHERE Username = ?");
			stmt.setString(1, userName);
			stmt.executeUpdate();
			try
			{
				stmt.close();
			}
			catch(SQLException e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			for(Item it:inventory)
			{
				stmt = server.getDatabaseConnection().getPreparedStatement("DELETE FROM ItemAttributes WHERE Item = ?");
				stmt.setInt(1, it.getID());
				stmt.executeUpdate();
				try
				{
					stmt.close();
				}
				catch(SQLException e)
				{
					server.getLog().error("Unabled to close Query: "+e.getMessage());
				}
				stmt = server.getDatabaseConnection().getPreparedStatement("INSERT INTO Items(Username, Type, Amount) VALUES (?, ?, ?)");
				stmt.setString(1, userName);
				stmt.setString(2, it.getName());
				stmt.setInt(3, it.getAmount());
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				if(rs.next())
				{
					int id = rs.getInt(1);
					try
					{
						stmt.close();
					}
					catch(SQLException e)
					{
						server.getLog().error("Unabled to close Query: "+e.getMessage());
					}
					for(String s:it.getAttributes().keySet())
					{
						stmt = server.getDatabaseConnection().getPreparedStatement("INSERT INTO ItemAttributes(Item, `Key`, `Value`) VALUES (?, ?, ?)");
						stmt.setInt(1, id);
						it.setID(id);
						stmt.setString(2, s);
						stmt.setString(3, it.getAttributes().get(s));
						stmt.executeUpdate();
						try
						{
							stmt.close();
						}
						catch(SQLException e)
						{
							server.getLog().error("Unabled to close Query: "+e.getMessage());
						}
					}
				}
				else
				{
					try
					{
						stmt.close();
					}
					catch(SQLException e)
					{
						server.getLog().error("Unabled to close Query: "+e.getMessage());
					}
				}
				rs.close();
			}
		}
		catch(SQLException e)
		{
			server.getLog().error("Unabled to save userdata: "+e.getMessage());
		}
		finally
		{
			try
			{
				stmt.close();
			}
			catch(SQLException e)
			{
				server.getLog().error("Unabled to close Query: "+e.getMessage());
			}
			
		}	
	}

	@Override
	public void onOpen(WebSocket origin)
	{
		
	}

	@Override
	public void onHandshake(WebSocket origin)
	{
		
	}

	@Override
	public void onHandshakeSuccessfull(WebSocket origin)
	{
		
	}

	/**
	 * Sends a message to the user and append a \n
	 * <p>
	 * @param string message to send
	 */
	public void sendLine(String string) {
		send(string + "\n");
	}
	
	/**
	 * @return whether the user is currently in a fight or wether not
	 */
	public boolean isInFight()
	{
		return fightIn != null;
	}
	
	/**
	 * @return the fight the user is currently in
	 */
	public Fight getFight()
	{
		return fightIn;
	}
	
	@Override
	public void attack()
	{
		startQuery(new QueryAttack(this));
	}

	/**
	 * @return the users Inventory as Item[] array
	 */
	public Item[] getInventory()
	{
		return inventory.toArray(new Item[]{});
	}
	@Override
	public void defend(Attack attack)
	{
		send(attack.getDescription()+ "Gelingt ihm das (<neutral>"+Main.toPercent(attack.getPRB())+"</neutral>), hat die Attacke (ATK: <atk>"+attack.getATK()+"</atk>)");
		startQuery(new QueryDefend(this));
	}

	@Override
	public float getAlive()
	{
		return getHP() / (float)getMaxHP();
	}

	@Override
	public void tellOutcome(Attack attack, Defense defense, int atk, int def, int rest)
	{
		send(defense.getDescription());
		if(atk != 0) send("\n<good>Die Attacke ist geglückt!</good>\n");
		else send("\n<bad>Die Attacke ging daneben</bad>\n");
		if(def != 0) send("<good>Die Verteidigung ist geglückt!</good>\n\n");
		else send("<bad>Die Verteidigung ging daneben</bad>\n\n");
		send("Angriffspunkte: <atk>"+atk+"</atk>, Verteidigungspunkte: <def>"+def+"</def>, es blieben <neutral>"+rest+"</neutral> Hitpoints übrig.\n\n");
	}

	@Override
	public boolean isDead()
	{
		return getHP() <= 0;
	}

	@Override
	public void endFightWinning()
	{
		setKey("Fights Won", ""+(getKeyAsInt("Fights Won")+1));
		send("Du hast den Kampf Gewonnen!\n");
		fightIn = null;
	}

	@Override
	public void endFightLosing()
	{
		setKey("Fights Lost", ""+(getKeyAsInt("Fights Lost")+1));
		send("Du hast den Kampf verloren!\n");
		fightIn = null;
	}

	@Override
	public float takeMoney()
	{
		int amount = (int) (getMoney() * 0.75F);
		doMoney(-amount);
		send("Du hast <money>"+amount+"€</money> an deinen Gegner verloren\n");
		return amount;
	}
	
	@Override
	public void endFightEscapedSelf(){
		send("Du bist entkommen!\n");
		fightIn = null;
	}
	
	@Override
	public void endFightEscapedEnemy(){
		send("Dein Gegner ist geflüchtet, die feige Sau!");
	}

	@Override
	public void giveMoney(float i)
	{
		send("Du hast <money>"+Main.roundTo(i,2)+"€</money> bekommen!\n");
		doMoney(i);
	}
	
	/**
	 * Sets the fight-attribute to the fight the user is fighting in
	 * <p>
	 * @param fight fight that the user is in
	 */
	public void startFight(Fight fight)
	{
		this.fightIn = fight;
	}

	@Override
	public void tellHP(Fighting opponent)
	{
		send("Du hast noch <hp>"+getHP()+"</hp> von <good>"+getMaxHP()+"</good> HP übrig ("+Main.toPercent(getAlive())+")\n");
		send(opponent.getName() + " hat noch <hp>"+opponent.getHP()+"</hp> von <good>"+opponent.getMaxHP()+"</good> HP übrig ("+Main.toPercent(opponent.getAlive())+")\n\n");
	}

	@Override
	public String getName()
	{
		return userName;
	}

	@Override
	public void dealDamage(int i)
	{
		hp = hp - i < 0 ? 0 : hp -i;
	}
		
	/**
	 * Returns the score of the user calculated by the kills/deaths he had in the arena
	 * <p>
	 * @return the score the user has 
	 */
	public int getArenaScore()
	{
		int k = getKeyAsInt("Fights Won");
		int d = getKeyAsInt("Fights Lost");
		d = d == 0 ? 1 : d;
		return (k*k + d*k)/d;
	}
}
