package org.cronosx.server;

import java.util.HashMap;

import org.cronosx.gta.Zone;
import org.cronosx.gta.zones.*;

/**
 * @author prior
 * Handles all zones.
 * Each zone should just have one instance loaded.
 * This manager manages these instances
 */
public class ZoneManager
{
	
	/**
	 * Instance of the main server
	 */
	private ServerGTA server;
	/**
	 * Zones registered
	 */
	private HashMap<Class<? extends Zone>, Zone> zones;
	public ZoneManager(ServerGTA server)
	{
		this.zones = new HashMap<Class<? extends Zone>, Zone>();
		this.server = server;
		loadZones();
	}
	
	/**
	 * All super-zones should be loaded here so at least some zones are preloaded as the server starts up
	 * Less important zones will be loaded on demand
	 */
	private void loadZones()
	{
		new ZoneInnenstadt(this);
			new ZoneArena(this);
			new ShopKleidung(this);
			new ShopFleischer(this);
			new ZoneAltstadt(this);
				new ShopKiosk(this);
				new ShopDöner(this);
			new ZoneGhetto(this);
				new PlaceKneipe(this);
				new ShopWeapons(this);
	}
	
	/**
	 * Gives you an zone-instance of the given class and caches this zone
	 * <p>
	 * Use this method to get instances of zones instead of creating new objects as there should be just one instance of each zone
	 * <p>
	 * @param zone Java-Reflection class object of the zone to grab
	 * @return instance of the zone
	 */
	public Zone getZone(Class<? extends Zone> zone)
	{
		if(!zones.containsKey(zone)) try
		{
			zones.put(zone, zone.getConstructor(this.getClass()).newInstance(this));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return zones.get(zone);
	}
	
	/**
	 * Returns the main-server instance
	 * @return the server
	 */
	public ServerGTA getServer()
	{
		return server;
	}
	
	/**
	 * registers an instance of a zone to its java-reflection class
	 * @param z zone to register
	 */
	public void registerZone(Zone z)
	{
		zones.put(z.getClass(), z);
		server.getLog().log("Registered Zone: "+z.getName());
	}
}
