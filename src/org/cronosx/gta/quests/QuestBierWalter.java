package org.cronosx.gta.quests;

import org.cronosx.gta.Item;
import org.cronosx.gta.Person;
import org.cronosx.gta.Quest;
import org.cronosx.gta.persons.PersonWalter;
import org.cronosx.server.User;

public class QuestBierWalter extends Quest
{
	private int bier = 0;
	public QuestBierWalter(User user)
	{
		super(user);
	}
	
	@Override
	public String getQuestTag()
	{
		return "QuestBierWalter";
	}
	
	@Override
	public String getName()
	{
		return "Bier für Walter";
	}
	
	@Override
	public String getDescription()
	{
		switch(getStage())
		{
			case 0:
				stage0();
				return "Ja, ich würde gerne mal eine Pause machen, aber dann weht das Laub über den ganzen Platz...\nKannst du mir nicht helfen? Danach können wir ja zusammen ein Bier trinken gehen.\nBlöder Wind. Er weht die Blätter schneller wieder auseinander als ich sie mit meinen kleinen Besen zusammenfegen kann.\nWenn du mir einen größeren Besen bringst, kann ich bald Pause machen.\n";
			case 1:
				//stepStage();
				return "Ah, der ist perfekt. Dankeschön. Nimm du doch meinen alten Besen. Zusammen sind wir schneller fertig.\n";
			case 2:
				stepStage();
				return "Endlich geschafft. Jetzt ist unsere schöne Stadt wieder sauber. Den alten Besen darfst du gerne behalten.\n Wollen wir zusammen Bier trinken? Ich darf hier allerdings nicht weg. Kannst du welches holen?\nDrüben im Ghetto in der Kneipe gibt es ziemlich gutes.\n";
			case 3:
				stage2();
				if(bier == 0)
					return "Du hast ja gar kein Bier mitgebracht. Wie sollen wir da Bier trinken? Drüben im Ghetto in der Kneipe gibt es ganz gutes.\n";
				else if(bier == 1)
					return "Hast du für dich etwa kein Bier mitgebracht? Ich will mein Bier nicht alleine trinken. Geh und hol dir auch eins.\n";
				else if(bier >= 2)
				{
					return "Lass uns trinken!\n";
				}
			case 4:
				stage3();
				return "Das hat gut getan. Danke. Jetzt muss ich aber weitermachen. War schön, mit dir zu plaudern. Auf Wiedersehen.\n";
			default:
				return "War schön mit dir Bier zu trinken.\n";
		}
	}
	
	public String getUserAnswer()
	{
		switch(getStage())
		{
			case 0:
				return "Willst du nicht mal Pause machen?\n";
			case 1:
				return "Hier, ich hab deinen Besen.\n";
			case 2:
				return "Fertig!\n";
			case 3:
				return "Hier: ein schönes, kaltes Bier\n";
			case 4:
				return "Man sieht sich\n";
			default:
				return "Jo\n";
		}
	}
	
	@Override
	public int getMinExp()
	{
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	public Class<? extends Person>[] getCharacters()
	{
		return new Class[] { PersonWalter.class };
	}
	
	@Override
	public void doQuest()
	{
		switch(getStage())
		{
			case 0:
				break;
			case 1:
				//user.send("Hilf mir fegen!");
				break;
			case 2:
				break;
			case 3:
				break;
			default:
				//user.send(getDescription());
		}
	}
	
	public void stage0()
	{
		if(user.inventoryContainsItem("Besen"))
		{
			stepStage();
			user.removeFromInventory("Besen");
			user.giveItem(new Item("Walters Besen", 1, user));
			try
			{
				registerCommand("Fegen", "fegen");
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void stage2()
	{
		if(user.inventoryContainsItem("Bier"))
		{
			bier = user.getInventoryItem("Bier").getAmount();
			if(bier >= 2) stepStage();
		}
	}
	
	public void stage3()
	{
		user.removeFromInventory("Bier", 2);
		user.send("Du setzt dich mit dem alten Walter auf die Bank unter dem Baum, der Quelle allen Übels, und genießt dein Bier im der noch wärmenden Herbstsonne. Walter bedankt sich für deinen Mühen mit |-green-|10€|-|\n");
		user.giveMoney(10);
		finishQuest();
	}
	
	public void fegen(String s)
	{
		if(user.inventoryContainsItem("Besen"))
		{
			user.send("Du fegst die Blätter zusammen\n");
			if(getStage() == 1)
			{
				stepStage();
			}
		}
		else
		{
			user.send("Wie willst du bitte ohne Besen fegen?");
		}
	}
}
