package org.cronosx.gta;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

/**
 * @author prior
 * Represents a zone, e.g. ghetto, city, a shop etc.
 */
public abstract class Zone
{
	/**
	 * Containing the users that are currently in this zone
	 */
	private CopyOnWriteArrayList<User> users;
	private ArrayList<Person> persons;
	protected ArrayList<Zone> subZones;
	protected ArrayList<ZoneShop> subShops;
	protected ArrayList<ZonePlace> subPlaces;
	/**
	 * Pointer to the global zone-manager
	 */
	protected ZoneManager zm;
	
	/**
	 * Registered commands for this particular zone
	 */
	private HashMap<String, Method> commands;
	
	/**
	 * @param zm instance of the global zone-manager
	 */
	public Zone(ZoneManager zm)
	{
		subZones = new ArrayList<Zone>();
		subPlaces = new ArrayList<ZonePlace>(); 
		subShops= new ArrayList<ZoneShop>();
		if(getSuperZone() != null) 
		{
			subZones.add(zm.getZone(getSuperZone()));
			zm.getZone(getSuperZone()).registerSubZone(this);
		}
		this.zm = zm;
		commands = new HashMap<String, Method>();
		zm.registerZone(this);
		users = new CopyOnWriteArrayList<User>();
		persons = new ArrayList<Person>();
	}
	
	/**
	 * Registers a subzone to this zone
	 * <p>
	 * @param z zone to register
	 */
	public void registerSubZone(Zone z)
	{
		if(z instanceof ZoneShop)
		{
			subShops.add((ZoneShop) z);
			subPlaces.add((ZonePlace) z);
		}
		else if(z instanceof ZonePlace)
			subPlaces.add((ZonePlace) z);
		else
			subZones.add(z);
		
	}
	
	/**
	 * Returns whether a user is in this zone or not
	 * <p>
	 * @param u user to test for
	 * @return if the user is here
	 */
	public boolean isUserHere(User u)
	{
		for(User u2:getUsers()) if(u2 == u) return true;
		return false;
	}
	
	/**
	 * Broadcast a string
	 * <p>
	 * Sends a String to all players that are currently in this zone
	 * <p>
	 * @param s string to send
	 */
	public void broadcast(String s)
	{
		for(User us:users)
		{
			us.send(s);
		}
	}
	
	public Person[] getPersons() {
		return persons.toArray(new Person[]{});
	}
	
	public Person getPerson(Class<? extends Person> clazz)
	{
		for(Person p:getPersons()) if(p.getClass().equals(clazz)) return p;
		return null;
	}
	
	public void addPerson(Person person)
	{
		persons.add(person);
	}

	/**
	 * Returns an User[] array containung all users that are currently in this zone
	 * @return users that are currently in this zone
	 */
	public User[] getUsers()
	{
		synchronized(users)
		{
			return this.users.toArray(new User[users.size()]);
		}
	}
	
	/**
	 * @return HashMap of commands that are registered in this zone
	 */
	public HashMap<String, Method> getCommands()
	{
		return commands;
	}
	
	/**
	 * Register a command for this particular zone
	 * <p>
	 * Registers the command to the given method. The method will be invoked when the command is entered by the user.
	 * The method has to be a public method containing exactly two parameters of the types User and String.
	 * The first parameter will be the user that invoked this command, the second one will be the exact command he entered
	 * <p>
	 * @param s command to register to
	 * @param m method to call
	 */
	protected void registerCommand(String s, String m)
	{
		try
		{
			commands.put(s, getClass().getMethod(m, User.class, String.class));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/**
	 * This method should be invoked when a user enters the zone.
	 * <p>
	 * Broadcasts a message to all users and registers the user to this zone.
	 * <p>
	 * @param user user that entered the zone
	 */
	public void enter(User user)
	{
		user.send("Du bist jetzt " + getNamePrefixed() + "\n");
		if(isSaveZone()) user.send("<good>Dies ist ein Sicheres Gebiet</good>\n");
		else user.send("<good>Hier ist es gefährlich. Man weiss nie, wer hinter der nächsten Ecke lauern könnte</good>\n");
		user.send("<description>"+getDescription()+"</description>");
		user.help("");
		synchronized(users)
		{
			for(User us:users)
			{
				us.send(user.getName() + " kommt um die Ecke\n");
			}
			users.add(user);
		}
		onEnter(user);
	}
	
	/**
	 * This method should be invoked when a user left this zone
	 * <p>
	 * Broadcasts a message to all users and unregisters the user from this zone.
	 * <p>
	 * @param user user that left the zone
	 */
	public void leave(User user)
	{
		synchronized(users)
		{
			users.remove(user);
			for(User us:users)
			{
				us.send(user.getName() + " hat sich verzogen\n");
			}
		}
	}
	
	/**
	 * Should return the pure name of this zone
	 * <p>
	 * @return the name of the zone
	 */
	public abstract String getName();
	
	/**
	 * Should return the prefixed name of this zone
	 * <p>
	 * Will be used for example at leave() and enter()
	 * If the name of your zone is "marketplace", this should be "in the marketplace"
	 * <p>
	 * @return the prefixed name of the zone
	 */
	public abstract String getNamePrefixed();
	
	/**
	 * Should return a Zone[] array with all zones that can be reached by car from here,
	 * if this returns null, it will be assumed that it is not possible to drive here
	 * <p>
	 * @return a Zone[] array containing all zones that can be reached by car from here
	 */
	public Zone[] getDirections()
	{
		return subZones.toArray(new Zone[]{});
	}
	/**
	 * Should return a ZoneShop[] array containing all zones that can be stolen from or be entered by walking
	 * if this returns null it will be assumed that there is nothing to steal from in this zone.
	 * Shops should return themselfs at this method so they can be stolen from from within themselfs
	 * <p>
	 * @return a ZoneShop[] array containing all zones that can be stolen from or be entered by walking
	 */
	public ZoneShop[] getShops()
	{
		return subShops.toArray(new ZoneShop[]{});
	}
	
	/**
	 * Should return a ZonePlace[] array of the places that can be reached by walking
	 * if this returns null, it will be assumed, that it is not possible to walk somewhere here.
	 * <p>
	 * @return a ZonePlace[] array of the places that can be reached by walking
	 */
	public ZonePlace[] getPlaces()
	{
		return subPlaces.toArray(new ZonePlace[]{});
	}
	/**
	 * Should return a more or less long description of the scene that describes the scene to the player.
	 * will be echoed to the player when he enters the scene.
	 * @return a description of the scene
	 */
	public abstract String getDescription();
	
	public boolean canDrive()
	{
		return getDirections() == null || getDirections().length == 0;
	}
	
	/**
	 * Should calculate and return whether this user can steal here.
	 * <p>
	 * @param user user to probe
	 * @return whether this user can steal here
	 */
	public abstract boolean canSteal(User user);
	
	/**
	 * Should calculate and return whether this user can see other players here.
	 * <p>
	 * If this returns false the "Umsehen"-command cannot be invoked
	 * <p>
	 * @param user user to probe
	 * @return whether this user can see other players here
	 */
	public abstract boolean canLook(User user);
	
	/**
	 * Will be invoked when a user enters this zone
	 * @param user user that entered this zone.
	 */
	public abstract void onEnter(User user);
	
	
	/**
	 * If a zone is a save zone it is not possible to launch a fight here
	 * at all
	 * <p>
	 * @return whether this is a save zone
	 */
	public abstract boolean isSaveZone();

	/**
	 * Should return this zones super-zone
	 * <p>
	 * The zone this place lies on. For example: If this place is registered to the zone "City" this should return the instance of the zone "City"
	 * <p>
	 * @return super-zone
	 */
	public abstract Class<? extends Zone> getSuperZone();
}
