package org.cronosx.gta;

import org.cronosx.gta.Fighting.Attack;
import org.cronosx.gta.Fighting.Defense;
import org.cronosx.server.User;

/**
 * @author prior
 * A Fight
 */
public class Fight
{
	protected Fighting player1;
	protected Fighting player2;
	protected Fighting nextToAttack;
	protected Fighting nextToDefend;

	protected Attack currentAttack;
	
	/**
	 * @param player1 the attacker
	 * @param player2 the attacked (first to defend)
	 */
	public Fight(Fighting player1, Fighting player2)
	{
		this.player1 = player1;
		this.player2 = player2;
		nextToAttack = player1;
		nextToDefend = player1;
	}
	
	/**
	 * Starts the first round
	 */
	public void startFight()
	{
		nextRound();
	}
	
	protected void nextRound()
	{
		currentAttack = null;
		if(nextToAttack == player1)
		{
			nextToAttack = player2;
			nextToDefend = player1;
		}
		else 
		{
			nextToAttack = player1;
			nextToDefend = player2;
		}
		nextToAttack.attack();
	}
	
	/**
	 * Called from the attacker when launching an attack
	 * <p>
	 * @param attacker attacker that executes the attack
	 * @param attack attack executed
	 * @throws WrongPlayerexception when it's not this objects turn to attack
	 */
	public void incomingAttack(Fighting attacker, Attack attack)
	{
		if(attacker == nextToAttack)
		{
			currentAttack = attack;
			nextToDefend.defend(attack);
		}
		else throw new WrongPlayerException("Wrong Attacker attacked."); 
	}
	
	/**
	 * Finds the opponent for the given player
	 * <p>
	 * @param player player to find opponent for
	 * @return the players opponent
	 */
	public Fighting getOpponent(Fighting player)
	{
		if(player == player1) return player2;
		else return player1;
	}
	
	/**
	 * Called when the defender launches his defense
	 * <p>
	 * @param defender defenders, that launches the defense
	 * @param defense defense launched by the defendes
	 * @throws WrongPlayerException when it's not this objects turn to defend now
	 */
	public void incomingDefense(Fighting defender, Defense defense)
	{
		if(defender == nextToDefend)
		{
			int atk = currentAttack.execute();
			int def = defense.execute();
			int rest = atk - def;
			rest = rest < 0 ? 0 : rest;
			defender.dealDamage(rest);
			nextToAttack.tellOutcome(currentAttack, defense, atk, def, rest);
			nextToDefend.tellOutcome(currentAttack, defense, atk, def, rest);
			nextToAttack.tellHP(nextToDefend);
			nextToDefend.tellHP(nextToAttack);
			roundEnded(nextToAttack, currentAttack, defense, atk, def);
		}
		else throw new WrongPlayerException("Wrong Defender defended.");
		if(defender.isDead())
		{
			fightEnded(nextToAttack, nextToDefend);
			nextToAttack.endFightWinning();
			nextToDefend.endFightLosing();
		}
		else
			nextRound();
	}

	/**
	 * Called, when the whole fight is over
	 * <p>
	 * @param winner winner of the fight
	 * @param loser loser of the fight
	 */
	public void fightEnded(Fighting winner, Fighting loser)
	{
		winner.giveMoney(loser.takeMoney());
	}
	
	/**
	 * Calles after a round ended
	 * <p>
	 * @param attacker player that attacked
	 * @param attack attack launched
	 * @param defense defense launched
	 * @param hpDealed hitpoints dealed by attacker
	 */
	public void roundEnded(Fighting attacker, Attack attack, Defense defense, int hpDealed, int hpRevoked)
	{
		
	}
	
	public void fliehen(User feigeSau)
	{
		Fighting enemy;
		if (feigeSau.equals(player1))
		{
			enemy = player2;
		}
		else if (feigeSau.equals(player2))
		{
			enemy = player1;
		}
		else throw new WrongPlayerException("A third person tries to escape a fight?");
		
		if(Math.random() < 0.5)
		{
			feigeSau.endFightEscapedSelf();
			enemy.endFightEscapedEnemy();
			return;
		}
		else 
		{
			nextRound();
		}
	}
	
	class WrongPlayerException extends RuntimeException
	{
		public WrongPlayerException(String string)
		{
			super(string);
		}

		private static final long	serialVersionUID	= 5506107623884085681L;	
	}
}
