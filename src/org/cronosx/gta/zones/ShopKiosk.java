package org.cronosx.gta.zones;

import org.cronosx.gta.*;
import org.cronosx.server.Main;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

public class ShopKiosk extends ZoneShop
{

	public ShopKiosk(ZoneManager zm)
	{
		super(zm);
		try
		{
			registerCommand("Snickers klauen", "klauSnickers");
			registerCommand("In der Nase popeln", "popelNase");
			registerCommand("Ein Lied singen", "singLied");
			registerCommand("Pornomagazine lesen", "liesPornos");
			registerCommand("Besen klauen", "klauBesen");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void klauBesen(User u, String string)
	{
		if(!u.inventoryContainsItem("Besen"))
		{
			u.send("Frag mich nicht wie, aber irgendwie habe ich es tatsächlich geschafft, den Besen umbemerkt zu klauen.\n");
			u.giveItem(new Item("Besen", u));
		}
		else
		{
			u.send("Ich habe doch schon einen Besen! Wozu die Mühe?\n");
		}
	}

	@Override
	public float getRiskFactor()
	{
		return 0.7F;
	}

	@Override
	public int getMaxWealth()
	{
		return 5;
	}

	@Override
	public int getMinWealth()
	{
		return 2;
	}
	
	public void popelNase(User u, String string)
	{
		u.send("Du popelst genüsslich in der Nase. Die Kassiererin guckt dich angewidert an.\n");
	}
	
	public void singLied(User u, String string)
	{
		broadcast(u.getName() + " singt mit " + Main.oneOf("grässlicher", "schriller", "heiserner", "blumiger") + " Stimme " 
				+ Main.oneOf("die Ode an die Freude", "Gangnam Style", "Alle meine Entchen", "die Nationalhymne", "ein Lied")+"\n");
		u.send("Alle scheinen seeehr begeistert zu sein...\n");
	}
	
	public void liesPornos(User u, String string)
	{
		u.send("Du schlägst den PlayBoy auf und siehst " + Main.oneOf("eine schöne Frau", "einen schönen Mann", "einen Hund", "eine Oma", "einen Opa", "deine Mutter", "deinen Vater")+
				" mit " + Main.oneOf("riesigen Brüsten", "einem knackigen Arsch")+ ".\n" +
				Main.oneOf(new String[]{"angeekelt", "erregt", "erleichtert", "schockiert", "zufrieden"}) + " schlägst du das Magazin wieder zu.\n");
	}
	
	public void klauSnickers(User user, String string)
	{
		String s = "Ja, für etwas anderes ist dieser Laden auch wirklich nicht zu gebrauchen!\n" +
					"Du versuchst dir unauffällig einen Snickers in die Tasche zu stecken und...\n";
		if(Main.randomize(calculateRisk(user, 0.5F)))
		{
			s += "kommst ungeschoren davon.\n";
			Item snickers = new Item("Snickers",1,user);
			snickers.setKey("HP", "5");
			snickers.setKey("Kategorie", "Essen");
			user.giveItem(snickers);
		}
		else
		{
			s += "die Kassiererin zieht dir die Ohren lang.\n" +
				"Du wurdest erwischt.\n";
		}
		user.send(s);
	}

	@Override
	public String getName()
	{
		return "Kiosk";
	}

	@Override
	public String getNamePrefixed()
	{
		return "im Kiosk";
	}
	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneAltstadt.class;
	}

	@Override
	public String getDescription()
	{ 
		return "Ein etwas heruntergekommener, kleiner Kiosk. Eigentlich gibt es hier außer einem Zeitungsständer mit Pornomagazinen, " +
				"dem Tresen mit der jungen, attraktiven Kassiererin und einer Auslage mit diversen Schokoriegeln nicht viel zu sehen." +
				"Was genau will ich eigentlich hier?\n";
	}

	@Override
	public boolean canDrive()
	{
		return false;
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
		
	}

	@Override
	public boolean isSaveZone()
	{
		return true;
	}
	
}
