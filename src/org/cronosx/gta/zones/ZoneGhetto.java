package org.cronosx.gta.zones;

import org.cronosx.gta.Zone;
import org.cronosx.server.Main;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;


public class ZoneGhetto extends Zone
{

	public ZoneGhetto(ZoneManager zm)
	{
		super(zm);
	}

	@Override
	public String getName()
	{
		return "Ghetto";
	}
	
	@Override
	public String getNamePrefixed()
	{
		return "im Ghetto";
	}

	@Override
	public String getDescription()
	{
		return "Ein richtiges Ghetto mit allem drum und dran... Ich habe sogar ein bisschen Angst, hier einfach so rumzulaufen. " +
				"Neben vielen alten Absteigen, Hochhäusern, die mehr als Baufällig aussehen gibt es hier noch eine alte Kneipe, " +
				"allerdings bin ich mir nicht so sicher, ob ich da reingehen möchte...\n"
				+ Main.oneOf(
						"Ein paar Kinder versuche grade " +Main.oneOf("eine Katze", "einen Fisch", "ein Baby", "einen Frosch", "ein Hündchen")+
						" mit " + Main.oneOf("Böllern", "Schokolade", "alten Zeitungen")+ " zu füttern. Armes Ding! \n",
						"Neben der Kneipe steht " + Main.oneOf("ein Jugendlicher", "ein Mädchen", "eine Nutte", "ein Polizist") +
						"und versucht " + Main.oneOf(new String[]{"Gras", "Koks", "Meth", "Mathebücher", "Crack"}) + "zu rauchen. \n",
						"Ein " + Main.oneOf("kleines Mädchen", "kleiner Junge", "alter Mann", "Teenager") + " kommt " + Main.oneOf("weinend", "schreiend", "lachend")+
						" aus einem Hauseingang gerannt und verschwindet in der Kneipe\n"
				);
	}

	@Override
	public boolean canDrive()
	{
		return true;
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
		
	}

	@Override
	public boolean isSaveZone()
	{
		return false;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneInnenstadt.class;
	}
	
}
