package org.cronosx.gta.zones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.cronosx.gta.*;
import org.cronosx.server.Main;
import org.cronosx.server.ServerGTA;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

import java.sql.PreparedStatement;

public class ZoneArena extends ZonePlace
{
	public static final int STATUS_CONFIRMED	= 1;
	public static final int STATUS_REQUESTED 	= 0;
	
	private ArrayList<Challenge> challenges;
	private Highscore highscore;
	
	public ZoneArena(ZoneManager zm)
	{
		super(zm);
		challenges = new ArrayList<Challenge>();
		importFromDB();
		registerCommand("Zum Duell Herausfordern", "forderHeraus");
		registerCommand("Herausforderungen anzeigen", "displayChallenges");
		registerCommand("Herausforderung annehmen", "confirmChallenge");
		registerCommand("Herausforderung ablehnen", "declineChallenge");
		registerCommand("Herausforderung zurückziehen", "revokeChallenge");
		registerCommand("Wette abschliessen", "placeBet");
		registerCommand("Wetten ansehen", "viewBets");
		registerCommand("Kampf beobachten", "obeyeFight");
		registerCommand("Duell austragen", "doChallenge");
		registerCommand("Highscore", "viewHighscore");
	}
	
	/**
	 * Called from the main Serverinstance when the usermanager is fully loaded
	 */
	public void loadHighscore()
	{
		highscore = new Highscore(zm.getServer());
	}
	
	/**
	 * Called from the API when the user used the "Wette Abschliessen" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void placeBet(User user, String string)
	{
		user.startQuery(new QueryPlaceBet(user));
	}

	/**
	 * Called from the API when the user used the "Wetten ansehen" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void viewBets(User user, String string)
	{
		user.startQuery(new QueryViewBets(user));
	}

	/**
	 * Called from the API when the user used the "Kampf beobachten" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void obeyeFight(User user, String string)
	{
		user.startQuery(new QueryObeyeFight(user));
	}
	
	/**
	 * Called when the user wants to see the highscore
	 * <p>
	 * @param user caller
	 * @param string ignored
	 */
	public void viewHighscore(User user, String string)
	{
		user.startQuery(new QueryHighscore(user));
	}

	/**
	 * Called from the API when the user used the "Herausforderung annehmen" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void confirmChallenge(User user, String string)
	{
		user.startQuery(new QueryConfirmChallenge(user));
	}

	/**
	 * Called from the API when the user used the "Herausforderung zurückziehen" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void revokeChallenge(User user, String string)
	{
		user.startQuery(new QueryRevokeChallenge(user));
	}

	/**
	 * Called from the API when the user used the "Zum Duell Herausfordern" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void forderHeraus(User user, String string)
	{
		user.startQuery(new QueryHerausfordern(user));
	}

	/**
	 * Called from the API when the user used the "Herausforderung ablehnen" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void declineChallenge(User user, String string)
	{
		user.startQuery(new QueryDeclineChallenge(user));
	}

	/**
	 * Called from the API when the user used the "Duell Austragen" command
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void doChallenge(User user, String string)
	{
		user.startQuery(new QueryDoChallenge(user, this));
	}
	
	@Override
	public String getName()
	{
		return "Arena";
	}

	@Override
	public String getNamePrefixed()
	{
		return "in der Arena";
	}

	@Override
	public String getDescription()
	{
		return "In der Arena können Kämpfe ausgetragen werden, " +
				"es kann herausgefordert werden und es können Wetten auf anstehende Kämpfe gesetzt werden. " +
				"Alle Ergebnisse werden festgehalten und in einer Highscore darsgestellt.";
	}

	@Override
	public boolean canSteal(User user)
	{
		return false;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
		displayChallenges(user, "");
	}

	/**
	 * Displays all challenges for a particular user
	 * <p>
	 * @param user caller
	 * @param string parameters (ignored)
	 */
	public void displayChallenges(User user, String string)
	{
		String offeneGestellte = "";
		String bestätigteGestellte = "";
		String offeneBekommene = "";
		String bestätigteBekommene = "";
		for(Challenge c:challenges)
		{
			if(c.player1 == user)
			{
				if(c.status == STATUS_REQUESTED)
				{
					offeneGestellte += "<bad>Unbestätigte</bad> Herausforderung <good>an</good> <neutral>"+c.player2.getName()+"</neutral> um <money>" + c.money + "€</money> (<bad>Noch nicht bestätigt</bad>)\n";
				}
				else if(c.status == STATUS_CONFIRMED)
				{
					bestätigteGestellte += "<good>Bestätigte</good> Herausforderung <good>an</good> <neutral>"+c.player2.getName()+"</neutral> um <money>" + c.money + "€</money> (" +
							(isUserHere(c.player2) ? "<good>Verfügbar</good>" : "<bad>Nicht verfügbar</bad>") + ")\n";
				}
			}
			if(c.player2 == user)
			{
				if(c.status == STATUS_REQUESTED)
				{
					offeneBekommene += "<bad>Unbestätigte</bad> Herausforderung <bad>von</bad> <neutral>"+c.player1.getName()+"</neutral> um <money>" + c.money + "€</money> (<bad>Noch nicht bestätigt</bad>)\n";
				}
				else if(c.status == STATUS_CONFIRMED)
				{
					bestätigteBekommene += "<good>Bestätigte</good> Herausforderung <bad>von</bad> <neutral>"+c.player2.getName()+"</neutral> um <money>" + c.money + "€</money> (" +
							(isUserHere(c.player1) ? "<good>Verfügbar</good>" : "<bad>Nicht verfügbar</bad>") + ")\n";
				}
			}
		}
		String s = offeneGestellte + bestätigteGestellte + offeneBekommene + bestätigteBekommene;
		s = s.length() == 0 ? "Garkeine\n" : s;
		user.send("Du hast folgende ausstehende Herausforderungen:\n" + s);
	}

	@Override
	public boolean isSaveZone()
	{
		return true;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneInnenstadt.class;
	}

	/**
	 * Removes a challenges from the stack of challenges,
	 * called if a fight has ended
	 * <p>
	 * @param c challenge to remove
	 */
	public void removeChallenge(Challenge c)
	{
		challenges.remove(c);
	}

	/**
	 * Imports all challenges and bets from database
	 */
	public void importFromDB()
	{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		PreparedStatement stmt2 = null;
		ResultSet rs2 = null;
		try
		{
			stmt = zm.getServer().getDatabaseConnection().getPreparedStatement("Select Player1, Player2, Done, Money, ID FROM Arena");
			rs = stmt.executeQuery();
			while(rs.next())
			{
				Challenge c = new Challenge(zm.getServer().getUserManager().getUser(rs.getString(1)),
											zm.getServer().getUserManager().getUser(rs.getString(2)),
											rs.getInt(3),
											rs.getInt(4));
				challenges.add(c);
				stmt2 = zm.getServer().getDatabaseConnection().getPreparedStatement("SELECT Amount, Username, BetOn FROM ArenaBets WHERE Fight = ?");
				stmt2.setInt(1, rs.getInt(5));
				rs2 = stmt2.executeQuery();
				while(rs2.next())
				{
					c.bets.add(new Bet(zm.getServer().getUserManager().getUser(rs2.getString(2)),
							zm.getServer().getUserManager().getUser(rs2.getString(3)),
							rs2.getInt(1), 
							c));
				}
			}
		}
		catch(SQLException e)
		{
			zm.getServer().getLog().error("Unabled to load Challenges: " + e.getMessage());
		}
		finally
		{
			try
			{
				rs.close();
				stmt.close();
				if(rs2 != null) rs2.close();
				if(stmt2 != null) stmt2.close();
			}
			catch(SQLException e)
			{
				zm.getServer().getLog().error("Critical: Unabled to close query: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Saves all challenges and bets to db
	 */
	public void exportToDB()
	{
		PreparedStatement stmt = null;
		try
		{
			stmt = zm.getServer().getDatabaseConnection().getPreparedStatement("DELETE FROM Arena");
			stmt.executeUpdate();
			stmt = zm.getServer().getDatabaseConnection().getPreparedStatement("DELETE FROM ArenaBets");
			stmt.executeUpdate();
			for(Challenge c:challenges)
			{
				stmt = zm.getServer().getDatabaseConnection().getPreparedStatement("INSERT INTO Arena(Player1, Player2, Money, Done) VALUES(?, ?, ?, ?)");
				stmt.setString(1, c.player1.getName());
				stmt.setString(2, c.player2.getName());
				stmt.setFloat(3, c.money);
				stmt.setInt(4, c.status);
				stmt.executeUpdate();
				ResultSet keys = stmt.getGeneratedKeys();
				for(Bet b:c.bets)
				{
					stmt = zm.getServer().getDatabaseConnection().getPreparedStatement("INSERT INTO ArenaBets(Fight, Username, BetOn, Money) VALUES(?, ?, ?, ?)");
					stmt.setInt(1, keys.getInt(1));
					stmt.setString(2, b.owner.getName());
					stmt.setString(3, b.betOn.getName());
					stmt.setFloat(4, b.money);
					stmt.executeUpdate();
				}
			}
		}
		catch(SQLException e)
		{
			zm.getServer().getLog().error("Could not export Challenges to database: "+e.getMessage());
		}
		finally
		{
			try
			{
				stmt.close();
			}
			catch(SQLException e)
			{
				zm.getServer().getLog().error("Critical: Unabled to close query: "+e.getMessage());
			}
		}
	}
	
	/**
	 * @author prior
	 * Represents a challenge from one user to another
	 * Hold bets, players, money set and the obeyers
	 */
	public class Challenge
	{
		public User player1;
		public User player2;
		public int status;
		public float money;
		public ArrayList<Bet> bets;
		public ArrayList<User> obeyers;
		
		public Challenge(User player1, User player2, int status, float money)
		{
			obeyers = new ArrayList<User>();
			bets = new ArrayList<Bet>();
			this.player1 = player1;
			this.player2 = player2;
			this.status = status;
			this.money = money;
		}
		
		/**
		 * Create a new Bet with the given parameters and passes it to the List of placed bets for this Challenge
		 * <p>
		 * @param owner user that placed the bet
		 * @param betOn user the user that bets is betting on
		 * @param money money the user bets
		 */
		public void placeBet(User owner, User betOn, float money)
		{
			bets.add(new Bet(owner, betOn, money, this));
		}
		
		/**
		 * Returns how many money was bet on the given user
		 * <p>
		 * @param user user to look up
		 * @return amount of money that was betted on this user 
		 */
		public float getBetsFor(Fighting user)
		{
			float amount = 0;
			for(Bet bet:bets)
			{
				if(user == bet.betOn)
				{
					amount += bet.money;
				}
			}
			return amount;
		}
		
		/**
		 * @return how many money was bet on this challenge overall
		 */
		public float getOverallBets()
		{
			float amount = 0;
			for(Bet bet:bets)
			{
				amount += bet.money;
			}
			return amount;
		}

		/**
		 * Tells all obeyers that the fight associated with this challenge has started
		 * <p>
		 * @param fightArena instance of arena
		 * @param player1 first player involved in the fight
		 * @param player2 second player involved in the fight
		 */
		public void announceFightBegan(FightArena fightArena,Fighting player1, Fighting player2)
		{
			for(User u: obeyers)
			{
				u.sendLine("Der Kampf zwischen <good>"+player1.getName()+"</good> und <bad>"+player2.getName()+"</bad> hat soeben begonnen!");
			}
		}

		/**
		 * Called from the fight when it has ended
		 * Tells all obeyers the outcome of the fight and calculates the bets
		 * <p>
		 * @param fightArena instance of arena
		 * @param winner player that won the fight associated with this challenge
		 * @param loser player that lost the fight associated with this challenge
		 */
		public void announceFightEnded(FightArena fightArena, Fighting winner,Fighting loser)
		{
			for(User u: obeyers)
			{
				u.sendLine("Der Kampf zwischen <good>"+player1.getName()+"</good> und <bad>"+player2.getName()+"</bad> hat geendet!\n" +
						"Spieler <good>"+winner.getName()+"</good> besiegte <bad>"+loser.getName()+"</bad>");
			}
			float overall = getOverallBets();
			float betswinner = getBetsFor(winner);
			for(Bet bet:bets)
			{
				if(winner == bet.betOn)
				{
					float part = bet.money/betswinner;
					float amount = (float) Main.roundTo(part * overall,2);
					bet.owner.doMoney(amount);
					bet.owner.sendLine("Deine Wette auf "+winner.getName()+" und "+loser.getName()+" brachte dir <money>"+ amount +"€</money> ein");
				}
			}
			highscore.updateUser((User)winner);
			highscore.updateUser((User)loser);
		}
		
	}
	
	/**
	 * @author prior
	 * Represents a bet that can be placed on a challenge
	 *
	 */
	public class Bet
	{
		User owner;
		User betOn;
		Challenge challenge;
		float money;
		public Bet(User owner, User betOn, float money, Challenge challenge)
		{
			this.money = money;
			this.owner = owner;
			this.betOn = betOn;
			this.challenge = challenge;
		}
	}
	
	/**
	 * @author prior
	 * Instanced if a player wants to create a new challenge
	 *
	 */
	public class QueryHerausfordern extends Query
	{
		private User challenged;
		public QueryHerausfordern(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			String s = "Du kannst eine Duell-Herausforderung an die folgenden Spieler stellen: \n";
			for(String us : zm.getServer().getUserManager().getUsernames())
			{
				if(!us.equals(user.getName())) s+="<command>"+us+"</command>\n";
			}
			user.sendLine(s + "<command>Abbrechen</command>\n");
			setAction("choice");
		}
		
		public void choice(String s)
		{
			boolean has = false;
			for(String us:zm.getServer().getUserManager().getUsernames())
			{
				if(us.toLowerCase().equals(s.toLowerCase()))
				{
					challenged = zm.getServer().getUserManager().getUser(us);
					for(Challenge c: challenges)
					{
						if((c.player1 == user && c.player2 == challenged) || (c.player2 == user && c.player1 == challenged))
						{
							user.sendLine("Zwischen euch besteht schon eine Herausforderung!");
							user.closeQuery();
							return;
						}
					}
					user.sendLine("Wie viel Geld willst du setzen? (Du hast <money>" + user.getMoney() + "€</money>)");
					setAction("getMoney");
					has = true;
					break;
				}
			}
			if(!has)
			{
				user.send("Den gibt es nicht. Hast du vielleicht immaginäre Freunde?\n" +
						"Versuch es härter!\n");
			}
		}
		
		public void getMoney(String s)
		{
			try
			{
				float money = Float.parseFloat(s);
				if(user.checkMoney(money))
				{
					challenges.add(new Challenge(user, challenged, STATUS_REQUESTED, money));
					user.send("Deine Herausforderung wird überbracht werden!\n");
					challenged.sendLine("<neutral>"+user.getName()+"</neutral> fordert dich heraus.");
					user.closeQuery();
				}
				else
				{
					user.sendLine("So viel Kohle hast du nicht!\n");
				}
			}
			catch(NumberFormatException e)
			{
				user.sendLine("Das ist doch keine Zahl\n" +
							  "Versuch es härter!");
			}
		}
	}
	
	/**
	 * @author prior
	 * instanced when a player wants to confirm a set challenge
	 *
	 */
	public class QueryConfirmChallenge extends Query
	{
		public QueryConfirmChallenge(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			String s ="Du wirst zurzeit von folgenden Spieler herausgefordert:\n";
			for(Challenge c : challenges)
			{
				if(c.player2 == user && c.status == STATUS_REQUESTED)
				{
					s += "<command>"+c.player1.getName() + "</command> (<money>" + c.money + "€</money>)\n";
				}
			}
			user.send(s+"Welchem Spieler willst du zusagen?\n<command>Abbrechen</command>\n");
			setAction("confirm");
		}
		
		public void confirm(String s)
		{
			for(Challenge c : challenges)
			{
				if(c.player2 == user && c.status == STATUS_REQUESTED && c.player1.getName().toLowerCase().equals(s.toLowerCase()))
				{
					if(user.checkMoney(c.money))
					{
						c.status = STATUS_CONFIRMED;
						user.send("Du hast die Herausforderung angenommen!\n");
						c.player1.sendLine("<neutral>"+c.player2.getName()+"</neutral> hat deine Herausforderung angenommen.");
						broadcast("<neutral>"+c.player2.getName()+"</neutral> hat eine Herausforderung von <neutral>"+c.player1.getName()+"</neutral> angenommen\n");
						user.closeQuery();
						return;	
					}
					else
					{
						user.sendLine("Nicht genug Geld.\n");
						user.closeQuery();
					}
				}
			}
			user.send("Der hat dich nicht herausgefordert. Dumme Nudel!\n Versuch es härter!\n");
		}
		
	}
	
	/**
	 * @author prior
	 * Instanced when a player wants to revoke a challenge he previously had set
	 *
	 */
	public class QueryRevokeChallenge extends Query
	{

		public QueryRevokeChallenge(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			String s ="Du stellst Herausforderungen an folgende Spieler: \n";
			for(Challenge c: challenges)
			{
				if(c.player1 == user)
				s+="<command>"+c.player2.getName()+"</command> (<money>" + c.money + "€</money>)\n";
			}
			user.send(s+"<command>Abbrechen</command>\n");
			setAction("revoke");
		}
		
		public void revoke(String s)
		{
			for(Challenge c: challenges)
			{
				if(c.player1 == user && c.player2.getName().toLowerCase().equals(s.toLowerCase()) && c.status == STATUS_REQUESTED)
				{
					challenges.remove(c);
					c.player2.sendLine("<neutral>"+user.getName()+ "</neutral> hat seine Herausforderung zurückgezogen!\n");
					user.sendLine("Herausforderung wurde zurückgezogen!\n");
					user.closeQuery();
					c.player1.doMoney(c.money);
					return;
				}
			}
			user.closeQuery();
			user.sendLine("Diesen Spieler forderst du doch garnicht heraus. Hast du Tomaten auf den Augen?\n Versuch es härter!\n");
		}
		
	}
	
	/**
	 * @author prior
	 * Instanced when a player wants to decline a challenge set upon him
	 *
	 */
	public class QueryDeclineChallenge extends Query
	{

		public QueryDeclineChallenge(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			String s ="Du wirst zurzeit von folgenden Spieler herausgefordert:\n";
			for(Challenge c : challenges)
			{
				if(c.player2 == user && c.status == STATUS_REQUESTED)
				{
					s += "<command>"+c.player1.getName() + "</command> (<money>" + c.money + "€</money>)\n";
				}
			}
			user.send(s+"Welchem Spieler willst du absagen?\n<command>Abbrechen</command>\n");
			setAction("decline");
		}
		
		public void decline(String s)
		{
			for(Challenge c : challenges)
			{
				if(c.player2 == user && c.status == STATUS_REQUESTED && c.player1.getName().toLowerCase().equals(s.toLowerCase()))
				{
					user.send("Du hast die Herausforderung abgelehnt!\n");
					c.player1.sendLine("<neutral>"+c.player2.getName()+"</neutral> hat deine Herausforderung abgelehnt.");
					challenges.remove(c);
					c.player1.doMoney(c.money);
					user.closeQuery();
					return;
				}
			}
			user.send("Der hat dich nicht herausgefordert. Dumme Nudel!\n Versuch es härter!\n");
		}
		
	}
	
	/**
	 * @author prior
	 * Instanced when a player wants to start a confirmed challenge
	 *
	 */
	public class QueryDoChallenge extends Query
	{
		private User other;
		private Challenge c;
		private ZoneArena arena;
		public QueryDoChallenge(User user, ZoneArena arena)
		{
			super(user);
			this.arena = arena;
		}

		@Override
		public void onQueryOpen()
		{
			String s = "Du hast bestätigte Duelle mit folgenden Spielern, die grade online und verfügbar sind:\n";
			for(Challenge c:challenges)
			{
				if((c.player1 == user || c.player2 == user) && c.status == STATUS_CONFIRMED)
				{
					User other;
					if(c.player1 == user) other = c.player2;
					else other = c.player1;
					if(!other.isInQuery() && isUserHere(other))
					{
						s+="<command>" + other.getName() + "</command>\n";
					}
				}
			}
			user.sendLine(s+"<command>Abbrechen</command>\nMit wem willst dus machen?");
			setAction("choice");
		}
		
		public void choice(String s)
		{
			for(Challenge c:challenges)
			{
				if((c.player1 == user || c.player2 == user) && c.status == STATUS_CONFIRMED)
				{
					User other;
					if(c.player1 == user) other = c.player2;
					else other = c.player1;
					if(other.getName().toLowerCase().equals(s.toLowerCase()))
					{
						if(isUserHere(other) && !other.isInQuery())
						{
							this.other = other;
							user.sendLine(other.getName()+" wird gefragt, bitte warte...\n");
							this.c = c;
							other.startQuery(new QueryJoinChallenge(other, this));
							return;
						}
					}
				}
			}
			user.sendLine("Entweder warst du mal wieder zu blöd zum tippen oder der Spieler kann grade nicht!");
		}
		
		public void confirmed()
		{
			user.sendLine(other.getName()+" hat zugestimmt!\n Kill it! Kill it with fire!");
			user.closeQuery();
			FightArena fight = new FightArena(c, arena);
			user.startFight(fight);
			other.startFight(fight);
			fight.startFight();
		}
		
		public void declined()
		{
			user.sendLine(other.getName()+" hat abgelehnt, das feige Huhn!");
			user.closeQuery();
		}
		
	}
	
	/**
	 * @author prior
	 * Instanced by QueryDoChallenge
	 * <p>
	 * Asks the other player if he wants to fight the challenge right now
	 */
	public class QueryJoinChallenge extends Query
	{
		private QueryDoChallenge query;
		public QueryJoinChallenge(User user)
		{
			super(user);
		}

		public QueryJoinChallenge(User user, QueryDoChallenge query)
		{
			super(user);
			this.query = query;
		}

		@Override
		public void onQueryOpen()
		{
			user.sendLine(query.getUser().getName() + " möchte jetzt das Duell mit dir austragen.\n" +
					"<command>Okay</command>\n" +
					"<command>Ne, verpiss dich</command>");
			setAction("confirm");
		}
		
		public void confirm(String s)
		{
			if(s.toLowerCase().equals("okay"))
			{
				user.sendLine("Bestätigt!");
				user.closeQuery();
				query.confirmed();
			}
			else
			{
				user.sendLine("Abgelehnt!");
				user.closeQuery();
				query.declined();
			}
		}	
	}
	
	/**
	 * @author prior
	 * Instanced when a player wants to obeye a challenge
	 *
	 */
	public class QueryObeyeFight extends Query
	{

		public QueryObeyeFight(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			String s = "Welchen Kampf willst du beobachten?\n";
			for(Challenge c: challenges)
			{
				if(c.status == STATUS_CONFIRMED && c.player1 != user && c.player2 != user)
				{
					s+= "<command>" + c.player1.getName() + " gegen " + c.player2.getName() + "</command>\n";
				}
			}
			user.sendLine(s);
			setAction("chooseFight");
		}
		
		public void chooseFight(String s)
		{
			for(Challenge c: challenges)
			{
				if(c.status == STATUS_CONFIRMED && c.player1 != user && c.player2 != user) 
				{
					if(s.toLowerCase().equals(c.player1.getName().toLowerCase() + " gegen " + c.player2.getName().toLowerCase()))
					{
						c.obeyers.add(user);
						user.sendLine("Ich hab ihn mir im Terminkalender angestrichen... Auch wenn ich noch nicht weiss, wann er statfindet!");
						user.closeQuery();
						return;
					} 
				}
			}
			user.sendLine("Dieser Kampf stand nicht zur Auswahl!\nVersuche es härter!\n");
		}
	}
	
	/**
	 * @author prior
	 * Instanced when a player wants to place a bet upon a challenge
	 *
	 */
	public class QueryPlaceBet extends Query
	{
		
		private Challenge c;
		private float money;
		
		public QueryPlaceBet(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			String s = "Auf welchen Kampf willst du wetten?\n";
			for(Challenge c: challenges)
			{
				if(c.status == STATUS_CONFIRMED && c.player1 != user && c.player2 != user) 
				{
					s+= "<command>" + c.player1.getName() + " gegen " + c.player2.getName() + "</command>\n";
				}
			}
			user.sendLine(s + "<command>Abbrechen</command>");
			setAction("chooseFight");
		}
		
		public void chooseFight(String s)
		{
			for(Challenge c: challenges)
			{
				if(c.status == STATUS_CONFIRMED && c.player1 != user && c.player2 != user) 
				{
					if(s.toLowerCase().equals(c.player1.getName().toLowerCase() + " gegen " + c.player2.getName().toLowerCase()))
					{
						this.c = c;
						user.sendLine("Wie viel Geld willst du wetten? Du hast <money>" + user.getMoney() + "€</money>");
						setAction("getAmount");
						return;
					}
				}
			}
			user.sendLine("Dieser Kampf stand nicht zur Auswahl!\nVersuche es härter!\n");
		}
		
		public void getAmount(String s)
		{
			try
			{
				float f = Float.parseFloat(s);
				if(user.checkMoney(f))
				{
					this.money = f;
					user.sendLine("Auf wen willst du wetten?\n" +
							"<command>"+c.player1.getName()+"</command>\n" +
							"<command>"+c.player2.getName()+"</command>");
					setAction("done");
				}
				else
				{
					user.sendLine("So viel Geld hast du nicht. Da musst du wohl auf dem Strich gehen!");
				}
			}
			catch(NumberFormatException e)
			{
				user.sendLine("Das ist keine Zahl!\nVersuche es härter!\n");
			}
		}
		
		public void done(String s)
		{
			User betOn;
			if(s.toLowerCase().equals(c.player1.getName()))
			{
				betOn = c.player1;
			}
			else if(s.toLowerCase().equals(c.player2.getName()))
			{
				betOn = c.player2;
			}
			else
			{
				user.sendLine("Der kämpft garnicht mit!\nVersuche es härter!");
				return;
			}
			c.placeBet(user, betOn, money);
			user.sendLine("<good>Wette abgeschlossen!</good>");
			user.closeQuery();
		}
		
	}
	
	/**
	 * @author prior
	 * Instanced when a player wants to view all bets that are placed on a challenge
	 *
	 */
	public class QueryViewBets extends Query
	{

		public QueryViewBets(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			String s = "Für welchen kampf möchtest du die Wetten sehen?\n";
			for(Challenge c: challenges)
			{
				if(c.status == STATUS_CONFIRMED) 
				{
					s+= "<command>" + c.player1.getName() + " gegen " + c.player2.getName() + "</command>\n";
				}
			}
			user.sendLine(s + "<command>Abbrechen</command>");
			setAction("chooseBetFight");
		}
		
		public void chooseBetFight(String s)
		{
			for(Challenge c: challenges)
			{
				if(c.status == STATUS_CONFIRMED) 
				{
					if(s.toLowerCase().equals(c.player1.getName().toLowerCase() + " gegen " + c.player2.getName().toLowerCase()))
					{
						user.sendLine("Es steht: <money>" + c.getBetsFor(c.player1) + "€</money> für "+c.player1.getName()+" und <money>" + c.getBetsFor(c.player2) + "€</money> für "+c.player2.getName());
						user.closeQuery();
						return;
					}
				}
			}
			user.sendLine("Dieser Kampf stand nicht zur Auswahl!\nVersuche es härter!\n");
		}
	}
	
	public class QueryHighscore extends Query
	{

		public QueryHighscore(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			user.sendLine("Welchen Platz will ich denn sehen?");
			setAction("platz");
		}
		
		public void platz(String s)
		{
			try
			{
				int index = Integer.parseInt(s);
				user.send(highscore.getHighscore(index));
				user.closeQuery();
			}
			catch(NumberFormatException e)
			{
				user.sendLine("Das ist keine Zahl. Versuche es härter!");
			}
		}
		
	}
	
	public class Highscore
	{
		private ServerGTA server;
		private Comparator<HighscoreEntry> cmp;
		private ArrayList<HighscoreEntry> hscr;
		public Highscore(ServerGTA server)
		{
			hscr = new ArrayList<HighscoreEntry>();
			this.server = server;
			cmp = new Comparator<HighscoreEntry>()
			{
				@Override
				public int compare(HighscoreEntry o1, HighscoreEntry o2)
				{
					return o2.score - o1.score;
				}
			};
			loadUsers();
		}
		
		public void loadUsers()
		{
			User[] users = server.getUserManager().getTemporaryArrayOfAllUsers();
			for(User user:users)
			{
				hscr.add(new HighscoreEntry(user));
			}
			users = null;
			Collections.sort(hscr, cmp);
		}
		
		public void updateUser(User user)
		{
			for(HighscoreEntry e : hscr)
			{
				if(e.username.equals(user.getName()))
				{
					e.score = user.getArenaScore();
					e.fightsLost = user.getKeyAsInt("Fights Lost");
					e.fightsWon =user.getKeyAsInt("Fights Won");
					e.fights = e.fightsWon + e.fightsLost;
					Collections.sort(hscr, cmp);
					return;
				}
			}
			hscr.add(new HighscoreEntry(user));
			Collections.sort(hscr, cmp);
		}
		
		public String getHighscore(int fromIndex)
		{
			fromIndex--;
			fromIndex = fromIndex >= hscr.size() ? hscr.size() - 1: fromIndex < 0 ? 0 : fromIndex;
			int endIndex = fromIndex + 10 >= hscr.size() ? hscr.size() - 1: fromIndex + 10;
			String s = "Highscore (Platz " + fromIndex + " bis " + endIndex + ")\n";
			for(int i = fromIndex; i < endIndex; i++)
			{
				HighscoreEntry e = hscr.get(i); 
				s+= String.format("%4d.Platz (%4d Punkte, %3d Kämpfe Gewonnen, %3d Kämpfe Verloren) ", i,e.score,e.fightsWon,e.fightsLost) + "<bad>" + e.username+"</bad>\n";
			}
			return s;
		}
	}
	
	public class HighscoreEntry
	{
		int score;
		String username;
		int fights;
		int fightsLost;
		int fightsWon;
		
		public HighscoreEntry(User user)
		{
			this.score = user.getArenaScore();
			this.username = user.getName();
			this.fightsLost = user.getKeyAsInt("Fights Lost");
			this.fightsWon =user.getKeyAsInt("Fights Won");
			this.fights = fightsWon + fightsLost;
		}
	}
}
