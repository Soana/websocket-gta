package org.cronosx.gta.zones;

import org.cronosx.gta.*;
import org.cronosx.gta.persons.PersonWalter;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

public class ZoneInnenstadt extends Zone
{
	public ZoneInnenstadt(ZoneManager zm)
	{
		super(zm);
		addPerson(new PersonWalter());
	}	

	@Override
	public String getName()
	{
		return "Innenstadt";
	}

	@Override
	public String getNamePrefixed()
	{
		return "in der Innenstadt";
	}

	@Override
	public String getDescription()
	{ 
		return "Die Innenstadt unserer kleinen Stadt. Der Herbst hat eingesetzt und überall liegen gelbe und rote Blätter von den Linden " +
				"am Straßenrand rum. Außer den üblichen Geschäften wie diversen Bäckern, einer Sparkasse, ein paar Kleidungsgeschäfte und " +
				"einem Elektrogeschäft kann man hier eigentlich nur Eis essen gehen. Obwohl es ein schöner Tag ist laufen kaum Leute hier rum. " +
				"Der alte Walter, den hier eigentlich jeder kennt ist grade damit beschäftigt, die Blätter wegzufegen, allerdings fallen schneller " +
				"neue herunter, als der arme Mann hinterher kommt. " +
				"Vielleicht hat er Lust, ein Bier mit mir zu trinken?\n";
	}

	@Override
	public boolean canDrive()
	{
		return true;
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
		
	}

	@Override
	public boolean isSaveZone()
	{
		return true;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return null;
	}
	
}
