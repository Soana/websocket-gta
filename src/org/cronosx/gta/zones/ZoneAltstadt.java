package org.cronosx.gta.zones;

import org.cronosx.gta.Zone;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;


public class ZoneAltstadt extends Zone
{

	public ZoneAltstadt(ZoneManager zm)
	{
		super(zm);
	}

	@Override
	public String getName()
	{
		return "Altstadt";
	}
	
	@Override
	public String getNamePrefixed()
	{
		return "in der Altstadt";
	}

	@Override
	public String getDescription()
	{
		return "Hier in der Altstadt steht neben einem alten Kiosk, von dem ich ehrlich gesagt keine Ahnung habe, wie der sich hier hält " +
				"noch ein wirklich teures Kaufhaus, das sich eigentlich keiner leisten kann. Es kommen aber oft reiche Leute aus " +
				"anderen Gegenden her, um sich hier Klamotten oder teures Spielzeug zu kaufen. \n";
	}

	@Override
	public boolean canDrive()
	{
		return true;
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
		
	}

	@Override
	public boolean isSaveZone()
	{
		return true;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneInnenstadt.class;
	}
	
}
