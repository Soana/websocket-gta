package org.cronosx.gta.zones;

import org.cronosx.gta.*;
import org.cronosx.server.Main;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

public class ShopDöner extends ZoneShop
{

	public ShopDöner(ZoneManager zm)
	{
		super(zm);
		registerCommand("Bestellen", "bestellen");
	}

	@Override
	public float getRiskFactor()
	{
		return 0.75F;
	}

	@Override
	public int getMaxWealth()
	{
		return 30;
	}

	@Override
	public int getMinWealth()
	{
		return 0;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneAltstadt.class;
	}

	@Override
	public String getName()
	{
		return "Sultan Döner";
	}

	@Override
	public String getNamePrefixed()
	{
		return "im Sultan";
	}
	@Override
	public String getDescription()
	{
		return "Ein typischer Dönerladen, wie man ihn vermutlich aus jeder Stadt kennt. Die Südländische Bedienung ist weiblich und hat" +
				" unheimliche riesen Hupen. Wouhouoou. Zu essen gibt es vermutlich Döner. Die Karte ist so mit Rechtschreibfehlern übersäht" +
				" und so klein geschrieben, dass ich kaum was lesen kann.\n" +
				"Auf "+Main.oneOf("dem Schafskäse", "dem Salat", "den Titten der Bedienung", "dem Fleisch", "dem Arsch der Bedienung")+ " " 
				+Main.oneOf("krabbelt", "sitzt", "klebt") +" "+ Main.oneOf("ein Marienkäfer", "eine Made", "ein Schmetterling", "eine Biene", "eine Fliege", "ein Kaugummie", "ein Schokoriegel")+"\n";
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
		
	}

	@Override
	public boolean isSaveZone()
	{
		return false;
	}
	
	public void bestellen(User user, String string)
	{
		user.startQuery(new QueryDöner(user));
	}
	
	public class QueryDöner extends Query
	{

		public QueryDöner(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			user.send("Guten Tag, Sie wünschen?\n");
			setAction("bestellen1");
		}
		
		public void bestellen1(String s)
		{
			if(s.toLowerCase().contains("döner"))
			{
				user.send("Mit scharfe Sauce?\n");
				setAction("bestellen2");
			}
			else user.send("Tut mir Leid, wir nix "+s+", wir nur Döner\n");
		}
		
		public void bestellen2(String s)
		{
			if(user.checkMoney(3.5F))
			{
				user.send("Das macht dann drei funfzig\n");
				user.doHP(user.getMaxHP()-user.getHP());
				user.send("Ich hoffen, Döner haben geschmeckt!\n");
			}
			else
			{
				user.send("Jungee, wenn du nix drei funfzig, nix döner, kapische?\n ");
			}
			user.closeQuery();
		}
		
	}
	
}
