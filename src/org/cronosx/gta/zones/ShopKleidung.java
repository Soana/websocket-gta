package org.cronosx.gta.zones;

import org.cronosx.gta.*;
import org.cronosx.server.Main;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

public class ShopKleidung extends ZoneShop
{

	public ShopKleidung(ZoneManager zm)
	{
		super(zm);
		try
		{
			registerCommand("Stöbern", "stoeber");
			registerCommand("Spannen", "spannen");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void spannen(User user, String string)
	{
		user.send("Du schaust heimlich in eine der Umkleidekabinen und gaffst " + Main.oneOf("einer Frau", "einem Mann", "einer Pappschachtel") + " auf " 
				+Main.oneOf("den nackten Arsch", "den hervorquellenden Busen", "den Fuß")+ ". " + Main.oneOf("Verwundert", "Verwirrt", "Befriedigt")+" ziehst du von dannen\n");
		
	}
	
	public void stoeber(User user, String s)
	{
		user.startQuery(new QueryKlamotten(user));
	}

	@Override
	public float getRiskFactor()
	{
		return 0.3F;
	}

	@Override
	public int getMaxWealth()
	{
		return 70;
	}

	@Override
	public int getMinWealth()
	{
		return 20;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneInnenstadt.class;
	}

	@Override
	public String getName()
	{
		return "Sonja Kleidet Ein!";
	}

	@Override
	public String getNamePrefixed()
	{
		return "im Sonja Kleidet Ein";
	}

	@Override
	public Zone[] getDirections()
	{
		return null;
	}

	@Override
	public ZoneShop[] getShops()
	{
		return null;
	}

	@Override
	public ZonePlace[] getPlaces()
	{
		return null;
	}

	@Override
	public String getDescription()
	{
		return "Ein Klamottenladen. Hier gibt es alles, außer vielleicht verünftige Badeanzüge. Vom Höschen bis zum Jackett ist hier alles vertreten.\n" +
				"Wenn ich an meinem Look arbeiten will, sollte ich mich hier vielleicht einmal umsehen.\n" +
				"Oh Mann, ist das da ein "+ Main.oneOf("bratwurstfarbenes", "goldenes", "plastik", "grün-pink kariertes") + " " + Main.oneOf("Häschen", "Top", "Jackett") + "?\n";
	}

	@Override
	public boolean canDrive()
	{
		return false;
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
		
	}

	@Override
	public boolean isSaveZone()
	{
		return true;
	}

	public class QueryKlamotten extends Query
	{
		private String[] kleidung;
		public QueryKlamotten(User user)
		{
			super(user);
		}
	
		@Override
		public void onQueryOpen()
		{
			user.send("Wo soll ich mich denn umsehen?\n" +
					"<command>Unterwäsche</command>\n" +
					"<command>Umstandsmode</command>\n" +
					"<command>Alltagskleidung</command>\n");
			setAction("kleidung");
		}
		
		public void kleidung(String s)
		{
			if(s.toLowerCase().equals("unterwäsche"))
			{
				user.send("Danke, ich glaube, ich habe genug Unterwäsche.");
				user.closeQuery();
			}
			else if(s.toLowerCase().equals("umstandsmode"))
			{
				user.send("Ääääh, was soll ich denn damit? Ich habe zwar in den letzten Monaten einen kleinen Schwimmring angesetzt, aber trotzdem dauert es noch etwas, bis ich hier gucken muss...\n");
				user.closeQuery();
			}
			else if(s.toLowerCase().equals("alltagskleidung"))
			{
				kleidung = new String[6];
				for(int i = 0; i < 6; i++)
					kleidung[i] = generateName(i < 3);
				user.send("Oha, die haben hier grad ein paar Sonderangebote für je 10€:\n");
				String s2 = "";
				for(String sb:kleidung)
					s2 += "<command>" + sb + "</command>\n";
				user.send(s2+"<command>Abbrechen</command>\n");
				setAction("kaufen");
			}
		}
		
		public void kaufen(String s)
		{
			boolean did = false;
			int i = 0;
			for(String s2:kleidung)
			{
				if(s2.toLowerCase().equals(s.toLowerCase()))
				{
					did = true;
					if(user.checkMoney(10))
					{
						user.send("Gekauft!");
						Item it = new Item(s2, user);
						it.setKey("Kategorie", "Kleidung");
						it.setKey("Badass", i < 3);
						user.giveItem(it);
						user.closeQuery();
					}
					else
					{
						user.send("Sieht aus, als hätte ich da kein Geld für übrig :(\n");
					}
				}
				i++;
			}
			if(!did)
			{
				user.send("Das kann man hier nicht kaufen, da muss ich mich wohl verguckt haben :/\n");
				user.closeQuery();
			}
		}
		
		private String generateName(boolean evil)
		{
			if(evil) return 
					Main.oneOf("schlabberig", "schwarz", "hauteng", "durchsichtig", "hässlich", "abgewetzt") + 
					Main.oneOf("e Lederjacke", "e Jogginghose", "es Unterhemd", "e Lederhose", "e Jeansjacke", "es Käppie", "er Kaputzenpullover", "es Höschen") +
					Main.oneOf(" mit Senfflecken", " mit Blutspritzern", " ohne Knöpfe", " mit Motorradganglogo", " mit Löchern", " mit Nieten", "", " mit Glitzersteinchen");
			else return		
					Main.oneOf("schick", "gut sitzend", "stylisch", "gemütlich") +		
					Main.oneOf("es Jackette", "es Kleid", "er Blazer", "er Pullover", "es Hemd", "es Shirt", "e Kordhose", "e Handtasche", "er Dress", "e Stoffhose")+
					Main.oneOf(" mit Perlen", " aus Samt", " mit Palietten", "");
		}
		
	}

}
