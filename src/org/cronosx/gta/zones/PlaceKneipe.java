package org.cronosx.gta.zones;


import org.cronosx.gta.*;
import org.cronosx.gta.persons.EnemyDrunkenMan;
import org.cronosx.server.Main;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

public class PlaceKneipe extends ZonePlace
{

	public PlaceKneipe(ZoneManager zm)
	{
		super(zm);
		try
		{
			registerCommand("Bestellen","kaufBier");
			registerCommand("Schlägerei anzetteln","schlägerei");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void schlägerei(User user, String s)
	{
		if(user.getInventory().length <= 1)
		{
			user.send("Ich sollte vielleicht nicht ohne Waffe angreifen...\n");
		}
		else
		{
			Enemy man =  new EnemyDrunkenMan();
			Fight fight = new Fight(user, man);
			user.startFight(fight);
			man.setFight(fight);
			fight.startFight();
		}
	}
	
	public void kaufBier(User user, String s)
	{
		user.startQuery(new QueryBier(user));
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneGhetto.class;
	}

	@Override
	public String getName()
	{
		return "Kneipe";
	}

	@Override
	public String getNamePrefixed()
	{
		return "in der Kneipe";
	}

	@Override
	public String getDescription()
	{
		return "Hier drinnen ist es ja fast noch schlimmer, als es von draußen ausgesehen hat. Leute torkeln besoffen durch die Tische, grölen. " +
				"Auf den Tischen Dreck, Erbrochenes oder komatöse \"Gäste\", falls man die überhaupt so nennen will. Es stinkt nach altem Fisch und " +
				"mir wird langsam echt übel. " +
				"Hat " + Main.oneOf("der Typ in der Ecke da", "die Kellnerin") +" grade " + Main.oneOf("gekotzt?", "Sex mit " + Main.oneOf("einem Fisch", "einer Nutte", "... was ist das überhaupt")+ "?", "nichts an?")+
				" Vielleicht bestell ich mir mal was zu trinken, oder vielleicht lieber nicht? ";
	}

	@Override
	public boolean canDrive()
	{
		return false;
	}

	@Override
	public boolean canSteal(User user)
	{
		return false;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
	}

	@Override
	public boolean isSaveZone()
	{
		return false;
	}
	

	public class QueryBier extends Query
	{
		private String itemID;
		private float value;
		public QueryBier(User user)
		{
			super(user);
		}
	
		@Override
		public void onQueryOpen()
		{
			user.send("Guten Abend!\n" +
					"Was trinkst du?\n" +
					"<command>Bier</command>\n" +
					"<command>Schnaps</command>\n" +
					"<command>Apfelschorle</command>\n" +
					"<command>Kakao</command>\n" +
					"<command>was anderes</command>... \n");
			setAction("kauf");
		}
		
		public void kauf(String s)
		{
			if(s.toLowerCase().equals("bier"))
			{
				user.send("Das macht 2€. Bar auf die Kralle. Angeschrieben wird nicht!\n" +
						"<command>Ja</command>/<command>Nein</command>\n");
				itemID = "Bier";
				value=2;
				setAction("kaufFertig");
			}
			else if(s.toLowerCase().equals("schnaps"))
			{
				user.send("1,50€ für nen Korn. Wenn dir das nicht passt, zieh Leine!\n" +
						"<command>Ja</command>/<command>Nein</command>\n");
				itemID = "Schnaps";
				value=1.5F;
				setAction("kaufFertig");
			}
			else if(s.toLowerCase().equals("apfelschorle"))
			{
				user.send("Solche Milchbubis wie dich können wir hier nicht gebrauchen. Verschwinde!\n");
				user.setZone(getSuperZone());
				user.closeQuery();
			}
			else if(s.toLowerCase().equals("kakao"))
			{
				user.send("Kakao will das kleine Ding. Hat man sowas schon gehört. Verzieh dich, bevor deine süßen Händchen noch schmutzig werden!\n");
				user.setZone(getSuperZone());
				user.closeQuery();
			}
			else
			{
				user.send("Du bist doch kein Bulle? Sonst hätte ich noch " +
						"<command>Dildo</command>: (<atk>ATK: 5</atk>, <def>DEF: 5</def>) für 10€ zu verkaufen\n" +
						"<command>Analysis-Skript</command>: (<atk>ATK: 3</atk>, <def>DEF: 8</def>) für 12€ oder\n" +
						"<command>Dingsbums</command>: (<atk>ATK:8</atk>, <def>DEF: 1</def>) für 9€. Was willst du denn haben?\n");
				setAction("sonstiges");
			}
		}
		
		public void sonstiges(String s)
		{
			if(s.toLowerCase().equals("dildo"))
			{
				itemID = "Dildo";
				user.send("Ok, also diesen gebrauchten Dildo für <money>10€</money>, ich würde den aber vorher sauber machen. Sicher, dass du den haben willst?\n" +
						"<command>Ja</command>\n" +
						"<command>Nein</command>\n");
				setAction("kaufFertig");
				value=10;
			}
			else if(s.toLowerCase().equals("analysis-skript"))
			{
				itemID = "Analysisskript";
				user.send("<money>12€</money> und dieses wundervolle, einzigartige Analysiskript ist dein!\n" +
						"<command>Ja</command>\n" +
						"<command>Nein</command>\n");
				setAction("kaufFertig");
				value=12;
			}
			else if(s.toLowerCase().equals("dingsbums"))
			{
				itemID = "Dingsbums";
				user.send("Ich würde dir mein Dingsbums ja für <money>9€</money> überlassen, aber du hast das nicht von mir, ist das klar?\n" +
						"<command>Ja</command>\n" +
						"<command>Nein</command>\n");
				setAction("kaufFertig");
				value=9;
			}
			else
			{
				user.send("Das stand nicht zur Auswahl.\n");
				user.closeQuery();
			}
		}
		
		public void kaufFertig(String s)
		{
			if(s.toLowerCase().equals("ja"))
			{
				if(user.checkMoney(value))
				{
					user.send("Deal!\n");
					Item it;
					if(itemID.equals("Dildo")) 
					{
						it = new Item("Dildo", user);
						it.setKey("Kategorie", "Waffe");
						it.setKey("ATK", 5);
						it.setKey("DEF", 5);
						it.setKey("PRB", 0.95F);
						user.giveItem(it);
					}
					else if(itemID.equals("Analysisskript")) 
					{
						it = new Item("Analysis-Skript", user);
						it.setKey("Kategorie", "Waffe");
						it.setKey("ATK", 3);
						it.setKey("DEF", 8);
						it.setKey("PRB", 0.95F);
						user.giveItem(it);
					}
					else if(itemID.equals("Dingsbums")) 
					{
						it = new Item("Dingsbums", user);
						it.setKey("Kategorie", "Waffe");
						it.setKey("ATK", 8);
						it.setKey("DEF", 1);
						it.setKey("PRB", 0.95F);
						user.giveItem(it);
					}
					else user.giveItem(new Item(itemID, user));
					user.closeQuery();
				}
				else
				{
					user.send("Willst du mich verarschen? So viel Geld hast du garnicht. Hau bloß ab!\n");
					user.closeQuery();
				}
			}
			else if(s.toLowerCase().equals("nein"))
			{
				user.send("Dann eben nicht!\n");
				user.closeQuery();
			}
			else
			{
				user.send("Das war eine simple Ja/Nein-Frage und sowas überfordert dich schon?\n");
			}
		}
		
	}

	
}
