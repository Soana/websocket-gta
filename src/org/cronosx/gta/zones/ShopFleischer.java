package org.cronosx.gta.zones;

import java.util.ArrayList;

import org.cronosx.gta.*;
import org.cronosx.server.Main;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

public class ShopFleischer extends ZoneShop
{

	public ShopFleischer(ZoneManager zm)
	{
		super(zm);
		registerCommand("Zum Tresen gehen", "tresen");
	}

	public void tresen(User user, String string)
	{
		user.startQuery(new QueryFleischer(user));
	}
	
	@Override
	public float getRiskFactor()
	{
		return 0.6F;
	}

	@Override
	public int getMaxWealth()
	{
		return 30;
	}

	@Override
	public int getMinWealth()
	{
		return 5;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneInnenstadt.class;
	}

	@Override
	public String getName()
	{
		// TODO Auto-generated method stub
		return "Fleis_her";
	}

	@Override
	public String getNamePrefixed()
	{
		// TODO Auto-generated method stub
		return "im Fleis_her";
	}
	@Override
	public String getDescription()
	{
		return "Wie lange fehlt jetzt eigentlich schon dieses c da oben?\n";
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
	}
	
	public class QueryFleischer extends Query
	{
		private ArrayList<String> auslage;
		private ArrayList<Float> preis;
		private ArrayList<Integer> hp;
		public QueryFleischer(User user)
		{
			super(user);
		}

		@Override
		public void onQueryOpen()
		{
			user.send("Willkommen beim Fleis_her! Wie kann ich dir weiterhelfen?\n");
			user.send("Ich möchte\n" +
					"<command>Fleisch kaufen</command>\n" +
					"<command>kein Fleisch kaufen</command>\n");
			setAction("kauf");
		}
		
		public void kauf(String s)
		{
			auslage = new ArrayList<String>();
			preis = new ArrayList<Float>();
			hp = new ArrayList<Integer>();
			String out = "Wir haben grade folgendes leckeres in der Auslage:\n";
			if(s.toLowerCase().equals("fleisch kaufen"))
			{
				for(int i = 0; i < 6; i++)
				{
					String flaisch = generateFleischName();
					float p = (float) Main.roundTo(Math.random()*7 + 3,2);
					int h = (int) (Math.random()*25 + 25);
					hp.add(h);
					preis.add(p);
					out+= "<command>" + flaisch + "</command> (<money>"+p+"€<money>) (<good>"+h+"HP</good>),\n";
					auslage.add(flaisch);
				}
				user.send(out+"<command>Abbrechen</command>\n");
				setAction("kaufJetzt");
			}
			else if(s.toLowerCase().equals("kein fleisch kaufen"))
			{
				for(int i = 0; i < 6; i++)
				{
					String flaisch = "kein "+generateFleischName();
					float p = (float) Main.roundTo(Math.random()*7 + 3 ,2);
					int h = (int) (Math.random()*25 + 25);
					hp.add(h);
					preis.add(p);
					out+= "<command>" + flaisch + "</command> (<money>"+p+"€<money>) (<good>"+h+"HP</good>),\n";
					auslage.add(flaisch);
				}
				user.send(out+"<command>Abbrechen</command>\n");
				setAction("kaufJetzt");
			}
			else
			{
				user.send("Tut mir Leid, so einen Schweinkram haben wir hier nicht!\n");
			}
		}
		
		public void kaufJetzt(String s)
		{
			for(int i = 0; i < auslage.size(); i++)
			{
				if(s.toLowerCase().equals(auslage.get(i).toLowerCase()))
				{
					if(user.checkMoney(preis.get(i)))
					{
						Item it = new Item(auslage.get(i), user);
						it.setKey("Kategorie", "Essen");
						it.setKey("HP", hp.get(i));
						user.giveItem(it);
						user.send("Viele Dank! Aber nicht alles auf einmal essen!\n");
						user.closeQuery();
						return;
					}
					else
					{
						user.send("Das kannst du dir nicht leisten! Such dir was anderes aus!\n");
					}
				}
			}
		}
		
		private String generateFleischName()
		{
			return Main.oneOf("alt", "frisch", "roh", "blutig", "lecker", "riesig", "würzig", "parniert", "gepökelt", "versalzen", "mariniert", "eingelegt")+
					Main.oneOf("es Schnitzel", "es Gulasch", "es Steak", "e Hackpfanne", "es Hacksteak", "es Gyros", "er Dönerspieß", "es Gammelfleisch", "es Putensteak",
							"es Stück totes Tier", "e Tomate", "er Apfel", "es Spanferkel", "e Hühnerflügelchen", "e Hähnchenschenkel", "es Ponysteak", "e Pferdewurst", "e Rossbratwurst", "es Fohlenfleisch");
		}
		
	}

	@Override
	public boolean isSaveZone()
	{
		return false;
	}
	
}
