package org.cronosx.gta.zones;

import java.util.ArrayList;

import org.cronosx.gta.*;
import org.cronosx.server.Main;
import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

public class ShopWeapons extends ZoneShop 
{

	public ShopWeapons(ZoneManager zm)
	{
		super(zm);
		registerCommand("Zum Tresen gehen", "gehTresen");
	}
	
	public void gehTresen(User user, String string)
	{
		user.startQuery(new QueryWeaponShop(user));
	}

	@Override
	public float getRiskFactor()
	{
		return 0.2F;
	}

	@Override
	public int getMaxWealth()
	{
		return 200;
	}

	@Override
	public int getMinWealth()
	{
		return 50;
	}

	@Override
	public Class<? extends Zone> getSuperZone()
	{
		return ZoneGhetto.class;
	}

	@Override
	public String getName()
	{
		return "Zum blutigen Phallus";
	}

	@Override
	public String getNamePrefixed()
	{
		return "im blutigen Phallus";
	}

	@Override
	public String getDescription()
	{
		return "Der Laden macht seinem Namen wirklich alle Ehre! Eine dunkle Absteige, voller Rauch mit einem riesigen Modell eines - nunja" +
				"blutigen Phalles über der Ladetheke, hier kann man allerlei wirklich abartigen Kram erkennen. Aus dem Hinterzimmer dringen leise " +
				"Schreie. Will ich wissen, wo die herkommen?\n";
	}

	@Override
	public boolean canSteal(User user)
	{
		return true;
	}

	@Override
	public boolean canLook(User user)
	{
		return true;
	}

	@Override
	public void onEnter(User user)
	{
	}

	@Override
	public boolean isSaveZone()
	{
		return false;
	}
	

	public class QueryWeaponShop extends Query
	{
	
		public QueryWeaponShop(User user)
		{
			super(user);
		}
		
		ArrayList<Weapon> weapons;
		
		private class Weapon
		{
			int value;
			int atk;
			int def;
			String name;
			float prb;
			public Weapon(int value, int atk, int def, String name, float prb)
			{
				this.atk = atk;
				this.def = def;
				this.name = name;
				this.value = value;
				this.prb = prb;
			}
		}
	
		@Override
		public void onQueryOpen()
		{
			user.send("Willkommen im blutigen Phallus!\n" +
					"Wie kann ich dir weiterhelfen?\n" +
					"<command>Waffen kaufen</command>");
			setAction("choice");
		}
		
		public void choice(String s)
		{
			if(s.toLowerCase().equals("waffen kaufen"))
			{
				weapons = new ArrayList<Weapon>();
				String s2 = "Wir haben allerlei Waffen in jeder Form und Farbe. Was gefällt dir denn am besten?\n";
				for(int i = 10; i < 1000; i+= 35)
				{
					int r = i+(int)(10*(Math.random()-0.5));
					r = r > 1000 ? 1000 : (r < 0 ? 0 :r);
					float a = (float) (((float)r/1000 + (Math.random()*0.2)-0.1)*120);
					float prb1 =(float) (Math.random()*0.67F);
					float prb3 = (float)(Math.random()* 0.33F);
					float prb2 =1- (prb1+prb3);
					int atk = (int) (a*prb1);
					int def = (int) (a*prb2);
					float prb = 0.4F + 2* prb3;
					String name = generateName(r);
					prb = prb > 1 ? 1: prb;
					weapons.add(new Weapon(r, atk, def,name, prb));
					s2 += "<command>"+ name + "</command> (ATK: <atk>" + atk + "</atk>) (DEF: <def>" + def + "</def>) (PRB: <neutral>" + Main.toPercent(prb) + "</neutral>) (<money>"+r+"€</money>),\n";
				}
				user.send(s2+"<command>Abbrechen</command>\n");
				setAction("buy");
			}
			else
				user.send("Du willst WAS!? Mach deine perversen Schweinerein gefälligst woanders!\n");
		}
		
		public void buy(String s)
		{
			for(Weapon weapon: weapons)
			{
				if(s.toLowerCase().equals(weapon.name.toLowerCase()))
				{
					if(user.checkMoney(weapon.value))
					{
						user.send("Verkauft! Aber stell damit keinen Unsinn an!\n");
						Item i = new Item(weapon.name, user);
						i.setKey("Kategorie", "Waffe");
						i.setKey("ATK",weapon.atk);
						i.setKey("DEF", weapon.def);
						i.setKey("PRB",weapon.prb);
						user.giveItem(i);
					}
					else
					{
						user.send("Das kannst du dir nicht leisten.\n");
					}
					user.closeQuery();
					return;
				}
			}	
			user.closeQuery();
			user.send("!? Das haben wir hier nicht.\n");
		}
		
		private String generateName(int awesomeness) // 1 - 1000.
		{
			
			String[] prefix = new String[]{
			"verrottet", "gammelig", "knuffig", "schimmlig", "blumig", "pink", "bröselig", "gehäkelt", "wollen", "weich", "flauschig", 
			"bescheiden", "hölzern", "leicht", "krass", "heftig", "rostig", "alt", "fabrikneu", "hakenkreuzig", 
			"komisch", "merkwürdig", "gemein", "fies", "hart", "schwer", "giftig", "metallen", "sexy", "affengeil",
			"abartig", "steinern", "gross", "brutal", "blutig", "krank", "zerfetzend", "gülden", "tödlich", "letal",
			"grausam", "schwarz", "magisch"};
			
			String[] subst = new String[]{
			"er Turnschuh", "es Stück Butter", "er Stiefel", "er Löffel", "er Kleiderbügel", "er Stock", "er Besenstiel", "er Stein", "e Häkelnadeln",
			"er Dildo", "e Pfanne", "er Golfschläger", "er Strick", "er Baseballschläger", "er Stromshocker", "e Peitsche", "e Lederpeitsche", 
			"es Messer", "e Axt", "er Degen", "er Säbel", "es Schwert", "er Bogen", "e Pistole", "e Armbrust", "er Zweihänder", "er Wurfstern", "es Wurfmesser",
			"e Kettensäge", "e AK-47", "e SMG", "es Säurefass", "er Laser", "e Bazooka", "e Hechselmaschine", "e Interkontinentalrakete"};
			
			/*String[] suffix = new String[]{
			"der Niedlichkeit", "der Liebe", "für Memmen", "des Himmels", "des Selbstmordes", "der Engel", "der Harmonie", 
			"deiner Mutter", "für den Arsch", "Karls des Großen", "der Waffen-SS", "Napoleons",
			"des Kampfes", "des Angriffs", "der Wut", "der Zerstörung", "der Vernichtung", "von irgendwas",
			"der Verdammung", "des Verdammten", "der Hölle", "des Teufels"};*/
	
			int a = (int) (awesomeness/(float)1000 * prefix.length + (Math.random() -0.5) * 3);
			a = a >= prefix.length ? prefix.length -1 : (a < 0 ? 0 : a);
			int b = (int) (awesomeness/(float)1000 * subst.length + (Math.random() -0.5) * 3);
			b = b >= prefix.length ? prefix.length -1 : (b < 0 ? 0 : b);
			/*int c = (int) (awesomeness/(float)1000 * suffix.length + (Math.random() -0.5) * 3);
			c = c >= prefix.length ? prefix.length -1 : (c < 0 ? 0 : c);*/
			return prefix[a>=prefix.length?prefix.length -1:a]+subst[b>=subst.length ? subst.length -1:b]/*+" "+suffix[c >= suffix.length ? suffix.length-1:c]*/;
		}
	}
}
