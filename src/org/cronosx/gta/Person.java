package org.cronosx.gta;


import java.util.HashMap;

import org.cronosx.gta.Talk;
import org.cronosx.gta.querys.QueryConversation;

public abstract class Person {
	/*protected ArrayList<Talk> person;
	protected ArrayList<Talk> user;
	
	public Person(){
		person = new ArrayList<Talk>();
		user = new ArrayList<Talk>();
	}

	public ArrayList<Talk> getPerson() {
		return person;
	}

	public ArrayList<Talk> getUser() {
		return user;
	}*/
	
	protected HashMap<String, Conversation> conversation;
	
	public Person(){
		conversation = new HashMap<String,Conversation>();
	}
	
	public HashMap<String,Conversation> getConversations() {
		return conversation;
	}
	
	public Conversation getConversation(String key){
		return conversation.get(key);
	}

	public abstract String getName();
	public abstract String getDescription();
	public abstract Talk getStandardExitLine();
	
	public Talk getExitLine(Conversation conversation){
		if(conversation.hasExitLine()){
			return conversation.getExitLine();
		}
		else{
			return getStandardExitLine();
		}
	}
	
	public void addTopLevelConversation(String starter, Talk... talk) {
		if(conversation.containsKey("Main")){
			Conversation main = conversation.get("Main");
			int firstDecision = 0;
			for(int i = 0; i < main.getLines().size(); i++){
				if(main.getLine(i).getNexts() > 0){
					firstDecision = i;
					break;
				}
			}
			Talk opening = main.getLine(firstDecision);
			opening.setNexts(opening.getNexts()+talk.length);
//			try {
//				talk[0].setMethod(QueryConversation.class.getMethod("enterConversation"));
//			}
//			catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			conversation.get("Main").addLine(talk[0]);
			
			addConversation(starter, talk);
		}
		else{
			conversation.put("Main", new Conversation("user", talk));
		}
	}
	
	public void addConversation(String starter,  Talk... talk){
		
		Conversation c = new Conversation(starter, talk);
		conversation.put(talk[0].getText(), c);
	}

	public void addConversation(String key, String starter,  Talk... talk){
		Conversation c = new Conversation(starter, talk);
		conversation.put(key, c);
	}
	
	
	/*public class Talk
	{
		private String text;
		private int[] nexts;
		private Method method;
		public Talk(String text, int... nexts)
		{
			this.text = text;
			this.nexts = nexts;
		}
		
		public Talk(String text, Method method, int... nexts)
		{
			this.method = method;
			this.text = text;
			this.nexts = nexts;
		}
		
		public int[] getNexts()
		{
			return nexts;
		}
		
		public String getText()
		{
			return text;
		}
		
		public boolean noMoreAnswers()
		{
			return nexts == null || nexts.length == 0;
		}
		
		public void invoke(Person person, User user)
		{
			if(method != null)
			{
				try
				{
					method.invoke(person, user);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}*/
}
