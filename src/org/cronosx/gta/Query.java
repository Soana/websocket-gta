package org.cronosx.gta;

import java.lang.reflect.Method;

import org.cronosx.server.User;

/**
 * @author prior
 * represents a query, e.g. a "discussion" between the player and the game
 */
public abstract class Query
{

	protected User user;
	private Method action;
	
	public Query(User user)
	{
		this.user = user;
	}
	
	public User getUser()
	{
		return user;
	}
	
	/**
	 * Sets the action to use next
	 * <p>
	 * The method set with this method will be called at the next userinput. It has to have exactly one parameter (String) the input will be
	 * passed through to
	 * <p>
	 * @param f method to call
	 */
	public void setAction(String f)
	{
		try
		{
			action = getClass().getMethod(f, String.class);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the method to be called at the next userinput
	 * <p>
	 * @return the next method to be called
	 */
	public Method getAction()
	{
		return action;
	}
	
	/**
	 * Has to be implemented
	 * <p>
	 * This method will be called when the query is fully opened by the user 
	 * and for example first text may be sent.
	 * Best to use to begin interaction
	 */
	public abstract void onQueryOpen();
	
	
	/**
	 * Called when the user entered some code
	 * @param s string entered by the user
	 */
	public void onCommand(String s)
	{
		try
		{
			action.invoke(this, s);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Override this and set return to false if this query cannot be aborted
	 * <p>
	 * @return whether this query can be aborted
	 */
	public boolean canAbort()
	{
		return true;
	}
}