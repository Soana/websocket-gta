package org.cronosx.gta.querys;

import org.cronosx.gta.Query;
import org.cronosx.gta.Zone;
import org.cronosx.server.User;

public class QueryDriveAround extends Query
{

	public QueryDriveAround(User user)
	{
		super(user);
	}

	@Override
	public void onQueryOpen()
	{
		if(user.getZone().getDirections() == null || user.getZone().getDirections().length == 0)
		{
			user.send("Keine Ahnung... Ich glaube hier komm ich mit dem Auto nirgends hin.\n");
			user.closeQuery();
		}
		else
		{
			String out = "Hier geht es hin: \n";
			for(Zone z : user.getZone().getDirections()) out += "<command>" + z.getName()+ "</command>\n";
			out += "<command>Abbrechen</command>\n";
			user.send(out);
			setAction("walkMenu");
		}
	}
	
	public void walkMenu(String s)
	{
		boolean found = false;
		if(s != null)
		for(Zone z:user.getZone().getDirections())
		{
			if(s.toUpperCase().equals(z.getName().toUpperCase()))
			{
				found = true;
				user.setZone(z.getClass());
			}
		}
		if(!found) user.send("Ähhh, wo willst du hin!?\n");
		user.closeQuery();
	}
	
}
