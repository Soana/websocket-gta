package org.cronosx.gta.querys;

import org.cronosx.gta.*;
import org.cronosx.server.Main;
import org.cronosx.server.User;

public class QuerySteal extends Query
{
	private ZoneShop zone;
	private float risk;
	public QuerySteal(User user)
	{
		super(user);
	}

	@Override
	public void onQueryOpen()
	{
		if(user.getZone().getShops() == null || user.getZone().getShops().length == 0)
		{
			user.send("Hier gibt es aber auch echt nichts zum klauen!!\n");
			user.closeQuery();
		}
		else
		{
			String out = "Es gibt hier diese Geschäfte: \n";
			for(ZoneShop z:user.getZone().getShops()) out += "<command>"+z.getName()+ "</command>\n";
			out += "<command>Abbrechen</command>\n";
			user.send(out);
			setAction("stealMenu");
		}
	}
	
	public void stealMenu(String s)
	{
		boolean did = false;
		if(s != null)
		{
			for(ZoneShop z:user.getZone().getShops())
			{
				if(s.toUpperCase().equals(z.getName().toUpperCase()))
				{
					zone = z;
					did = true;
				}
			}
			if(did)
			{
				user.send("Wie hast du dir das gedacht? \n" +
						"<command>1</command> Reinschleichen und Mopsen                <neutral>(30% Risiko)</neutral>\n" +
						"<command>2</command> Rumschreien und Betteln                  <neutral>(40% Risiko)</neutral>\n" +
						"<command>3</command> Bewaffneter Überfall                     <neutral>(60% Risiko)</neutral>\n" +
						"<command>4</command> Kassierin niederschlagen und Geld nehmen <neutral>(80% Risiko)</neutral>\n");
				setAction("stealMenu2");		
			}
			else 
			{
				user.send("So einen Laden gibt es hier nicht. Putz dir die Brille!\n");
				user.closeQuery();
			}
		}
		else 
		{
			user.send("So einen Laden gibt es hier nicht. Putz dir die Brille!\n");
			user.closeQuery();
		}
	}
	
	public void stealMenu2(String s)
	{
		if(s.equals("1")) risk = 0.3F;
		else if(s.equals("2")) risk = 0.4F;
		else if(s.equals("3")) risk = 0.6F;
		else if(s.equals("4")) risk = 0.8F;
		else 
		{
			user.send("Das klingt irgendwie komisch... aber lassen wirs drauf ankommen.\n");
			risk = (float) Math.random();
		}
		user.send("Bei einer Erfahrung von " + Main.toPercent(user.getKeyAsFloat("ExpKlauen")) + "\n" +
				  "und einer Schwierigkeit von " + Main.toPercent(zone.getRiskFactor()) + "\n" +
				  "hast du eine Erfolgswahrscheinlichkeit von <neutral>" + Main.toPercent(zone.calculateRisk(user, risk))+ "</neutral>!\n" +
				  "Ziehst du das Ding durch? \n" +
				  "<command>Ja</command>\n" +
				  "<command>Nein</command>\n");
		setAction("stealMenu3");
	}
	
	public void stealMenu3(String s)
	{
		if(s.toUpperCase().equals("JA")) 
		{
			if(Main.randomize(zone.calculateRisk(user, risk)))
			{
				float geld = (float) Main.roundTo((((zone.getMaxWealth() - zone.getMinWealth()) * Math.random() + zone.getMinWealth())*risk),2);
				user.doMoney(geld);
				user.send("Geile Sache, du hast <money>"+ geld + "</money> Euro erbeutet.\n");
				if(Main.randomize(risk)) 
				{
					float expWin = (float)(Math.random()/50);
					user.setKey("ExpKlauen", ""+(user.getKeyAsFloat("ExpKlauen")+expWin));
					user.send("Du hast <good>"+Main.toPercent(expWin)+"</good> Experiences im Bereich Diebstahl bekommen!");
				}
			}
			else
			{
				user.send("Scheisse... Das ging daneben.\n");
			}
		}
		else 
		{
			user.send("Du besinnst dich und ziehst leise von dannen\n");
		}
		user.closeQuery();
	}
}
