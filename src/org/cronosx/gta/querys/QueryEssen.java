package org.cronosx.gta.querys;

import org.cronosx.gta.Item;
import org.cronosx.gta.Query;
import org.cronosx.server.User;

public class QueryEssen extends Query
{

	public QueryEssen(User user)
	{
		super(user);
	}

	@Override
	public void onQueryOpen()
	{
		String s ="";
		boolean has = false;
		for(Item it:user.getInventory())
		{
			if(it.isFood())
			{
				has = true;
				s+="    <command>"+it.getName()+"</command> x"+it.getAmount()+"(HP: <good>"+it.getAsInt("HP")+"<good>)\n";
			}
		}
		if(has)
		{
			user.send("Du trägst folgende Leckereien mit dir rum:\n" + s);
			setAction("ess");
		}
		else
		{
			user.send("Du hast nix zu essen, da musst du wohl verhungern\n");
			user.closeQuery();
		}
	}
	
	public void ess(String s)
	{
		for(Item it:user.getInventory())
		{
			if(it.getName().toLowerCase().equals(s.toLowerCase()))
			{
				user.send("Omnomnomnomnom\n");
				user.doHP(it.getAsInt("HP"));
				user.removeFromInventory(s);
				user.closeQuery();
				return;
			}
		}
		user.send("Ääh, das kann man doch nicht essen!!\n");
	}
	
}
