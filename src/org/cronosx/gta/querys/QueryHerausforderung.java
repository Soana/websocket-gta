package org.cronosx.gta.querys;

import org.cronosx.gta.Fight;
import org.cronosx.gta.Query;
import org.cronosx.server.User;

public class QueryHerausforderung extends Query
{

	public QueryHerausforderung(User user)
	{
		super(user);
	}

	@Override
	public void onQueryOpen()
	{
		if(user.getInventory().length <= 1)
		{
			user.send("Ich sollte vielleicht nicht ohne Waffe angreifen...\n");
			user.closeQuery();
		}
		else
		{
			String su = "Folgende Leute stehen hier so rum: \n    ";
			boolean jemandDa = false;
			for(User u:user.getZone().getUsers())
			{
				if(u != user && u.getInventory().length >= 1)
				{
					su += "<command>"+u.getName() +"</command>, ";
					jemandDa = true;
				}
			}
			if(jemandDa)
			{
				user.send(su.substring(0, su.length() -2)+"\n");
				setAction("forderHeraus");
			}
			else
			{
				user.send("Hier ist keiner der als Gegener in Frage kommt," +
						" soll ich Geister oder Wehrlose verkloppen?\n");
				user.closeQuery();
			}
		}
	}
	
	public void forderHeraus(String s)
	{
		for(User u:user.getZone().getUsers())
		{
			if(s.toLowerCase().equals(u.getName().toLowerCase()))
			{
				user.closeQuery();
				Fight fight = new Fight(u, user);
				u.startFight(fight);
				user.startFight(fight);
				fight.startFight();
				return;
			}
		}
		user.closeQuery();
		user.send("Diese Person ist hier nicht...\n");
	}
	
}
