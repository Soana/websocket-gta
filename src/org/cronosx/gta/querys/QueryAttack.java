package org.cronosx.gta.querys;

import org.cronosx.gta.Item;
import org.cronosx.gta.Query;
import org.cronosx.gta.Fighting.Attack;
import org.cronosx.server.Main;
import org.cronosx.server.User;

public class QueryAttack extends Query
{
	public QueryAttack(User user)
	{
		super(user);
	}

	@Override
	public void onQueryOpen()
	{
		String s ="\nDu musst angreifen!\n" +
				  "Folgendes aus deinem Inventar kannst du benutzen: \n";
		for(Item i: user.getInventory())
		{
			s += "<command>" + i.getName() + "</command> (ATK: <good>"+i.getATK()+"</good>) (PRB: <neutral>" + Main.toPercent(i.getPRB())+"</neutral>), \n";
		}
		s += "\n |-yellow-||url|Fliehen|/url||-|";
		
		user.send(s+"\n");
		
		setAction("attack");
	}
	
	public void attack(String s)
	{
		for(final Item i:user.getInventory())
		{
			if(s.toLowerCase().equals(i.getName().toLowerCase()))
			{
				user.closeQuery();
				Attack attack = new Attack()
				{
					@Override public String getDescription() { return user.getName() + " greift dich mit " + i.getName() + " an\n";}
					@Override public String getName() { return "Mit "+i.getName()+ " angreifen"; }
					@Override public int getATK() { return i.getATK(); }
					@Override public float getPRB() { return i.getPRB(); }	
				};
				user.getFight().incomingAttack(user, attack);
				return;
			}
		}
		user.send("Das stand nicht zur Auswahl! Versuch es härter!\n");
	}
	
	@Override
	public boolean canAbort()
	{
		return false;
	}
}
