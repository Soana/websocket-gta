package org.cronosx.gta.querys;

import org.cronosx.gta.Query;
import org.cronosx.gta.*;
import org.cronosx.server.User;

public class QueryWalkAround extends Query
{

	public QueryWalkAround(User user)
	{
		super(user);
	}

	@Override
	public void onQueryOpen()
	{
		if(user.getZone().getPlaces() == null || user.getZone().getPlaces().length == 0)
		{
			user.send("Hier kann man nirgendwo hinlaufen... Schade.\n");
			user.closeQuery();
		}
		else
		{
			String out =  "Hier kannst du hinlaufen: \n";
			for(ZonePlace z : user.getZone().getPlaces()) out += "<command>" + z.getName()+ "</command>\n";
			out +="<command>Abbrechen</command>\n";
			user.send(out);
			setAction("walkMenu");
		}
		
	}
	
	public void walkMenu(String s)
	{
		boolean found =false;
		if(s != null)
		for(ZonePlace z: user.getZone().getPlaces())
		{
			if(s.toUpperCase().equals(z.getName().toUpperCase()))
			{
				found = true;
				user.setZone(z.getClass());
			}
		}
		if(!found) user.send("So einen Ort kann ich hier garnicht finden!\n");
		user.closeQuery();
	}
	
}
