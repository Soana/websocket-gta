package org.cronosx.gta.querys;

import org.cronosx.gta.Item;
import org.cronosx.gta.Query;
import org.cronosx.gta.Fighting.Defense;
import org.cronosx.server.Main;
import org.cronosx.server.User;

public class QueryDefend extends Query 
{
	public QueryDefend(User user)
	{
		super(user);
	}

	@Override
	public void onQueryOpen()
	{
		String s ="\n\nDu musst dich verteidigen!\n" +
				  "Folgendes aus deinem Inventar kannst du benutzen: \n";
		for(Item i: user.getInventory())
		{
			s += "<command>" + i.getName() + "</command> (DEF: <good>"+i.getDEF()+"</good>) (PRB: <neutral>" + Main.toPercent(i.getPRB())+"</neutral>), \n";
		}
		s += "\n |-yellow-||url|Fliehen|/url||-|";

		user.send(s+"\n");
		setAction("defend");
	}
	
	public void defend(String s)
	{
		for(final Item i:user.getInventory())
		{
			if(s.toLowerCase().equals(i.getName().toLowerCase()))
			{
				user.closeQuery();
				Defense defense = new Defense()
				{
					@Override public String getDescription() { return user.getName() + " verteidigt sich mit " + i.getName() + "\n";}
					@Override public String getName() { return "Mit "+i.getName()+ " verteidigen"; }
					@Override public int getDEF() { return i.getDEF(); }
					@Override public float getPRB() { return i.getPRB(); }	
				};
				user.getFight().incomingDefense(user, defense);
				return;
			}
		}
		user.send("Das stand nicht zur Auswahl! Versuch es härter!\n");
	}
	
	@Override
	public boolean canAbort()
	{
		return false;
	}
}
