package org.cronosx.gta.querys;


import java.util.ArrayList;

import org.cronosx.gta.Person;
import org.cronosx.gta.Conversation;
import org.cronosx.gta.Query;
import org.cronosx.gta.Talk;
import org.cronosx.server.User;

public class QueryConversation extends Query {

	private Person person;
	private ArrayList<Conversation> konversationen;
	private ArrayList<Integer> indices;
	private boolean leaveAll = false;
	private boolean suppressPersonAnswer = false;
	//private int[] lastAnswers;
	
	public QueryConversation(User user) {
		super(user);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onQueryOpen() {
		if(user.getZone().getPersons() == null || user.getZone().getPersons().length == 0)
		{ 
			user.send("Hier gibt es niemanden... Außer du willst unbedingt mit dem Boden reden\n");
			user.closeQuery();
		}
		else
		{
			String out = "Wen willst du ansprechen? \n";
			for(Person p:user.getZone().getPersons()){
				out += "\t<command>"+p.getName()+ "</command>\n";
			}
			out += "\t<command>Abbrechen</command>\n";
			
			user.send(out);
			setAction("ansprechenMenu");
		}
	}
	
	public void ansprechenMenu(String s)
	{
		boolean did = false;
		if(s != null)
		{
			for(Person p:user.getZone().getPersons())
			{
				if(s.toUpperCase().equals(p.getName().toUpperCase()))
				{
					person = p;
					did = true;
				}
			}
			if(did)
			{	
				konversationen = new ArrayList<Conversation>();
				konversationen.add(person.getConversation("Main"));
				indices = new ArrayList<Integer>();
				indices.add(1);
				
				user.sendLine("Du:\t" + getText(0));
				user.sendLine(person.getName() + ":\t" + getText(1));
				displayOptions();
			}
			else 
			{
				user.send("Den gibt's hier nicht!\n");
				user.closeQuery();
			}
		}
		else 
		{
			user.send("Den gibt's hier nicht!\n");
			user.closeQuery();
		}
		
	}
	
	public void antwortMenu(String s){
		int eingabe = -1;
		try{
			eingabe = Integer.parseInt(s);
		}
		catch(NumberFormatException e){
			user.sendLine("Das ist keine Zahl! Probier es nochmal");
			setAction("antwortMenu");
		}
//		user.sendLine(konversationen.toString());
		if(eingabe < 1 || eingabe > konversation().getLine(index()).getNexts()+1){
			user.sendLine("Das stand nicht zur Auswahl! Probier es nochmal");
			setAction("antwortMenu");
		}
		
//		user.sendLine(Integer.toString(index()));
		userAnswer(eingabe);
//		user.sendLine("index" + Integer.toString(index()) + " length" + konversation().getLines().size() + " " + konversation().getLine(index()).getText());
//		user.sendLine(Integer.toString(index()) + " " + konversation());
		if(!suppressPersonAnswer){
			personAnswer(konversation().getLine(index()));
		}
		else{
			displayOptions();
			suppressPersonAnswer = false;
		}
		user.sendLine(konversationen.toString());
	}
	
	private void userAnswer(int eingabe){
		if(eingabe > konversation().getLine(index()).getNexts()){
			String userAnswer = person.getExitLine(konversation()).getText();
			if(userAnswer.equals("suppress") || userAnswer.startsWith("QUEST")){
				userAnswer = person.getExitLine(konversation()).invoke(null);
			}
			user.sendLine("Du:\t" + userAnswer);
			if(person.getExitLine(konversation()).getNexts() == 0){
				suppressPersonAnswer = true;
			}
		}
		else{
			String userAnswer = getText(index()+eingabe);
			if(userAnswer.equals("suppress") || userAnswer.startsWith("QUEST")){
				userAnswer = konversation().getLine(index()+eingabe).invoke(null);
			}
			user.sendLine("Du:\t" + userAnswer);

		}
		
//		user.sendLine("acknowledged");
		
		if(konversation().getLine(index()).getNexts() > 1 
				&& konversation().getLine(index()+eingabe).getNexts() != 0){

			if(!konversation().getLine(index()).isMultipleChoice()){
				indices.set(0, -1);

			}

			if(konversationen.size() == 1 && (index()+eingabe) == (konversation().getLines().size()-1)){
				leaveAll = true;
			}

			enterConversation(getText(index()+eingabe));
			if(konversation().getLine(index()) == konversation().getLastLine()){
				suppressPersonAnswer = true;
			}
		}
		else if(eingabe >= konversation().getLines().size() || index()+eingabe >= konversation().getLines().size()){
//			user.send("128");
			suppressPersonAnswer = true;
			leaveConversation();

		}
		else if(konversation().getLine(index()+eingabe).getNexts() == 0){
			if(konversation().getLine(index()+eingabe) == konversation().getLastLine()){
				suppressPersonAnswer = true;
			}
			leaveConversation();
		}
		else{
			indices.set(0, index()+1);
		}
		
	}
	
	private void personAnswer(Talk choice){
		int personChoice = 1;
		if(choice.getNexts() > 1){
			personChoice = (int) (Math.random()*choice.getNexts() + 1);
//			user.sendLine("Auswahl für Person: " + Integer.toString(personChoice));
		}
//		user.sendLine("personChoice" + Integer.toString(personChoice));
		String personAnswer = getText(index()+personChoice);
		if(personAnswer.equals("suppress") || personAnswer.startsWith("QUEST")){
			personAnswer = konversation().getLine(index()).invoke(null);
		}
		user.sendLine(person.getName() + ":\t" + personAnswer);
		
		konversation().getLine(index()+personChoice).invoke(getText(index()+personChoice));
		
		if(choice.getNexts() > 1){
//			user.sendLine("multiple choice");
			
		
//			if(konversationen.size() == 1 && (index()+personChoice) == (konversation().getLines().size()-1)){
//				leaveAll = true;
//			}
			int index = index();
			indices.set(0, -1);
//			user.sendLine(Integer.toString(konversation().getLine(index+personChoice).getNexts()) + " " + konversation().getLine(index+personChoice).getText());

			if(konversation().getLine(index+personChoice).getNexts() != 0){
				enterConversation(getText(index+personChoice));
//				user.sendLine(Integer.toString(konversation().getLine(index()).getNexts()));
				user.sendLine(Integer.toString(index) + " " + Integer.toString(personChoice) + " " + konversation().getLine(index+personChoice).getText());
				if(konversation().getLine(index()).getNexts() == 0){
//					user.send("167");
					leaveConversation();
				}
			}
			else{
				user.sendLine(Integer.toString(index) + " " + Integer.toString(personChoice) + " " + konversation().getLine(index+personChoice).getText());
				if(konversation().getLine(index+personChoice).getNexts() == 0){
//					user.send("167");
					leaveConversation();
				}
			}
		}
		else{
//			user.sendLine("single choice");
			indices.set(0, index()+1);
			if(konversation().getLine(index()).getNexts() == 0){
				leaveConversation();
			}
		}
		
		

		if(!leaveAll){	
			displayOptions();
		}
	}
	
	private void displayOptions(){
//		user.sendLine(indices.toString());
		int i = 0;
		for(;i < konversation().getLine(index()).getNexts(); i++){
			String option = getText(index()+1+i);
			if(option.equals("suppress") || option.startsWith("QUEST")){
				System.out.println(getText(index()+1+i));
				option = konversation().getLine(index()+1+i).invoke(null);
			}
			user.sendLine("\t<command>" + (i+1) + "</command> " + option);
		}
		String exit = person.getExitLine(konversation()).getText();
		if(exit.equals("suppress") || exit.startsWith("QUEST")){
			exit = konversation().getLine(index()).invoke(null);
		}
		user.sendLine("\t<command>" + (i+1) + "</command> " + exit);
		setAction("antwortMenu");
	}
	
	public void enterConversation(String key){
		konversationen.add(0, person.getConversation(key));
		indices.add(0, new Integer(0));
//		user.sendLine("enter:" + key);
	}
	
	public void leaveConversation(){
//		user.sendLine("leave");
		if(konversationen.size() <= 1 || leaveAll){
			user.closeQuery();
			suppressPersonAnswer = true;
			return;
		}
		konversationen.remove(0);
		indices.remove(0);
		if(index() == -1){
//			user.send("index -1 ");
			leaveConversation();
		}
	}
	
	public String getText(int index){
		return konversationen.get(0).getLine(index).getText();
	}

	public int index(){
		return indices.get(0);
	}

	public Conversation konversation(){
		return konversationen.get(0);
	}
}