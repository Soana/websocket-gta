package org.cronosx.gta;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.cronosx.server.User;

public abstract class Quest {
	
	protected String tag;
	protected int stage;
	protected HashMap<String, Method> commands;
	protected User user;
	
	public String getTag() {
		return tag;
	}
	
	public int getStage()
	{
		return stage;
	}
	
	public void stepStage()
	{
		stage++;
		user.setKey(getTag(), stage+"");
		doQuest();
	}
	
	public void setStage(int i)
	{
		stage = i;
		doQuest();
	}
	public void finishQuest()
	{
		user.send("<good>Quest abgeschlossen:</good> "+getName()+"\n");
		user.setKey(getTag(), "Finished");
		user.removeQuest(this);
	}
	
	/**
	 * Each Quest has to be logged in the users profile by adding a tag for the quest to the "quest"-key in the users keys
	 * @param user
	 */
	public Quest(User user) 
	{
		commands = new HashMap<String, Method>();
		tag = getQuestTag();
		this.user = user;
	}
	
	/**
	 * Register a command for this particular quest
	 * <p>
	 * Registers the command to the given method. The method will be invoked when the command is entered by the user.
	 * The method has to be a public method containing exactly one parameters of the type String.
	 * The first parameter will be the user that invoked this command, the second one will be the exact command he entered
	 * <p>
	 * @param s command to register to
	 * @param m method to call
	 */
	protected void registerCommand(String s, String m)
	{
		try
		{
			commands.put(s, getClass().getMethod(m, String.class));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public HashMap<String, Method> getCommands()
	{
		return commands;
	}
	
	public abstract String getQuestTag();
	
	/**
	 * Gives the name of the quest
	 * @return name of the quest
	 */
	public abstract String getName();
	
	/**
	 * Gives the description of the single tasks in the quest, starting at task 0, the first task you are given
	 * @param task the number of the task
	 * @return the description of this particular task
	 */
	public abstract String getDescription();
	
	/**
	 * Gives the User prompt in conversation with the character for a specific task
	 * @param task number of task (starting at 0)
	 * @return user prompt
	 */
	public abstract String getUserAnswer();

	/**
	 * Gives the minimum experience-points you need for a specific task of the quest
	 * @param task the number of the task (starting at 0)
	 * @return the minimum exp
	 */
	public abstract int getMinExp();
	
	/**
	 * gives the characters you need for each task of a quest
	 * @param task the number of the task (starting at 0)
	 * @return an array of characters you need for the specific task of the quest
	 */
	public abstract Class<? extends Person>[] getCharacters();
	
	/**
	 * Manages the flow of the quests, like checking whether the condition for the active task is met and setting the next task
	 * Therefore a specific key has to be registered in the users keys, which saves his progress in the quest
	 * @param user the user doing the quest
	 */
	public abstract void doQuest();
}
