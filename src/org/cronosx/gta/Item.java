package org.cronosx.gta;

import java.util.HashMap;

import org.cronosx.server.User;

/**
 * @author prior
 * represents an item or a group of items with the same attributes
 */
public class Item
{
	private int id;
	private String name;
	private int amount;
	protected HashMap<String, String> attributes;
	public User owner;

	public Item(String name, int amount, User owner)
	{
		this.name = name;
		this.amount = amount;
		this.owner = owner;
		this.attributes = new HashMap<String, String>();
	}
	
	public Item(String name, User owner)
	{
		this.name = name;
		this.amount = 1;
		this.owner = owner;
		this.attributes = new HashMap<String, String>();
	}
	
	/**
	 * This constructor will mainly be used to save and load serialized Items from the database.
	 * <p>
	 * You should not use this constructor to instance a new item as the id refers to the unique databaseid
	 * whose change could have massive sideeffects
	 * <p>
	 * @param id database-id
	 * @param name name this item has 
	 * @param amount amount of items
	 * @param attributes a HashMap of attributes this item has
	 * @param owner an instance of user indicating the owner of this item
	 */
	public Item(int id, String name, int amount, HashMap<String, String> attributes, User owner)
	{
		this.id = id;
		this.name = name;
		this.amount = amount;
		this.attributes = attributes;
		this.owner = owner;
	}

	/**
	 * @return the amount of these items
	 */
	public int getAmount()
	{
		return amount;
	}

	/**
	 * Set the amount
	 * <p>
	 * Changes the amount of these items absolutely
	 * <p>
	 * @param amount amount to set to
	 */
	public void setAmount(int amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the owner of this item
	 */
	public User getOwner()
	{
		return owner;
	}

	/**
	 * Set the owner for the object
	 * @param owner owner to set to
	 */
	public void setOwner(User owner)
	{
		this.owner = owner;
	}

	/**
	 * @return the unique database-id
	 */
	public int getID()
	{
		return id;
	}

	/**
	 * @param key to set for
	 * @param o value for the key (will be converted to String)
	 */
	public void setKey(String key, Object o)
	{
		attributes.put(key, o.toString());
	}
	
	/**
	 * @param key key to look up
	 * @return the value for the key as String
	 */
	public String getAsString(String key)
	{
		return attributes.containsKey(key) ? attributes.get(key) : "";
	}
	
	/**
	 * @param key key to look up
	 * @return the vlaue for the key parsed as integer
	 * @throws NumberFormatException when the value is not a valid integer
	 */
	public int getAsInt(String key)
	{
		return attributes.containsKey(key) ? Integer.parseInt(attributes.get(key)) : 0;
	}
	
	/**
	 * @param key key to look up
	 * @return the value for the key parsed as float
	 * @throws NumberFormatException when the value is not a valid number 
	 */
	public float getAsFloat(String key)
	{
		return attributes.containsKey(key) ? Float.parseFloat(attributes.get(key)) : 0F;
	}
	
	/**
	 * @param key key to look up
	 * @return the value for the key parsed as boolean
	 */
	public boolean getAsBoolean(String key)
	{
		return attributes.containsKey(key) ? Boolean.parseBoolean(attributes.get(key)) : false;
	}
	
	/**
	 * @return the items name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Set database-id
	 * <p>
	 * Use with caution! Changing the database-id may cause sideeffects to the players inventory and the item-attributes.
	 * <p> 
	 * @param i id to set
	 */
	public void setID(int i)
	{
		id = i;
	}
	
	/**
	 * @param key key to check for
	 * @return whether the attributes contain this key or not 
	 */
	public boolean hasKey(String key)
	{
		return attributes.containsKey(key);
	}

	/**
	 * Returns the raw-data-strings of the attributes
	 * <p>
	 * Do not use when not neccessary, use getters instead
	 * <p>
	 * @see getAsBoolean
	 * @see getAsFloat
	 * @see getAsInt
	 * @see getAsString
	 * @see hasKey
	 * @return hashmap containing the raw-data of the attributes
	 */
	public HashMap<String, String> getAttributes()
	{
		return attributes;
	}
	
	/**
	 * Changes the amount
	 * <p>
	 * Changes the amount of these items relatively to their current value
	 * <p>
	 * @param i amount of change
	 */
	public void doAmount(int i)
	{
		setAmount(getAmount() + i);
	}
	
	/**
	 * @return whether the item is cloth
	 */
	public boolean isCloth()
	{
		return getAsString("Kategorie").equals("Kleidung");
	}
	
	/**
	 * @return whether the item is a weapon
	 */
	public boolean isWeapon()
	{
		return getAsString("Kategorie").equals("Waffe");
	}
	
	/**
	 * @return whether the item is a drug
	 */
	public boolean isDrug()
	{
		return getAsString("Kategorie").equals("Droge");
	}
	
	/**
	 * @return whether the item is a book
	 */
	public boolean isBook()
	{
		return getAsString("Kategorie").equals("Buch");
	}
	
	/**
	 * @return whether the item is food
	 */
	public boolean isFood()
	{
		return getAsString("Kategorie").equals("Essen");
	}
	
	/**
	 * @return whether the Item is a drink
	 */
	public boolean isDrink()
	{
		return getAsString("Kategorie").equals("Getränk");
	}
	
	/**
	 * Get the attack-value of this item
	 * <p>
	 * Higher values indicate higher attack-points and vice-verse. If the attack-key is set, it will be returned, otherwise a
	 * standard-value depending on the item-category will be used.
	 * <p>
	 * @return attack as integer
	 */
	public int getATK()
	{
		if(hasKey("ATK")) return getAsInt("ATK");
		else
		{
			if(isCloth()) return 11;
			else if(isWeapon()) return 12;
			else if(isDrug()) return 1;
			else if(isBook()) return 7;
			else if(isFood()) return -5;
			else if(isDrink()) return 7;
			else return 3;
		}
	}
	
	
	/**
	 * Get the probability of a success in attack or defense
	 * <p>
	 * The value is between 0 and 1 as a floatingpoint number, where 0 indicates a 0percent chance of the attack or defense.
	 * If the probability-key is set, it will be returned. Otherwise, standard-values depending on the item-category will be used.
	 * <p>
	 * @return probability as float
	 */
	public float getPRB()
	{
		if(hasKey("PRB")) return getAsFloat("PRB");
		else
		{
			if(isCloth()) return 0.15F;
			else if(isWeapon()) return 0.9F;
			else if(isDrug()) return 0.7F;
			else if(isBook()) return 0.85F;
			else if(isFood()) return 1;
			else if(isDrink()) return 0.4F;
			else return 0.5F;
		}
	}
	
	/**
	 * Get the defense-value of this item
	 * <p>
	 * returns the defense-value of the item as an integer, where bigger number are a higher defense-value and vice-verse
	 * If the defense-key is set in the attributes, it will be returned. otherwise there will be used standardv-alues depending
	 * on the item-category
	 * <p>
	 * @return defense as integer
	 */
	public int getDEF()
	{
		if(hasKey("DEF")) return getAsInt("DEF");
		else
		{
			if(isCloth()) return 4;
			else if(isWeapon()) return 6;
			else if(isDrug()) return 0;
			else if(isBook()) return 5;
			else if(isFood()) return 1;
			else if(isDrink()) return 1;
			else return 3;
		}
	}
}
