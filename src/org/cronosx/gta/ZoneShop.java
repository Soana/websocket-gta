package org.cronosx.gta;


import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

/**
 * @author prior
 * Represents a shop that can be entered or stolen from
 * (Nope, shops are good for nothing but to be entered and stolen from. 
 * Please build you shopping-query by yourself)
 */
public abstract class ZoneShop extends ZonePlace
{
	
	/**
	 * @param zm instance of the global zone-manager
	 */
	public ZoneShop(ZoneManager zm)
	{
		super(zm);
		subShops.add(this);
	}
	
	/**
	 * Should return the risk-factor
	 * <p>
	 * Represents the risk of stealing to fail. Higher values indicate that it is more easy to steal from this shop and vice-verse
	 * Will also be used to calculate gained experiences
	 * <p>
	 * @return the riskfactor as a float
	 */
	public abstract float getRiskFactor();
	/**
	 * Should return a higher number than getMinWealth
	 * <p>
	 * Represents the maximum value to be stolen from this shop at best conditions
	 * <p>
	 * @return max amount of money to be stolen
	 */
	public abstract int getMaxWealth();
	/**
	 * Represents the least value to be stolen from this shop at best conditions
	 * @return minimum amount of money to be stolen
	 */
	public abstract int getMinWealth();
	
	/**
	 * Calculates the overall risk of the stealing to succeed, depending on the risk the user wanted to use, the risk-factor of this shop
	 * and the users experiences
	 * Higher values indicate a higher probability of the user to fail
	 * <p> 
	 * @param u user to refer to
	 * @param risk risk the user wanted to use
	 * @return the overall probability of the stealing to succeed
	 */
	public float calculateRisk(User u, float risk)
	{
		float exp = u.getKeyAsFloat("ExpKlauen");
		if(exp <= 0.1) 
		{
			exp = 0.1F;
			u.setKey("ExpKlauen", ""+exp);
		}
		float finalFac =(getRiskFactor() / (1-exp)) * (1-risk) * 2 ;
		return finalFac > 1 ? 1: finalFac;
	}
}
