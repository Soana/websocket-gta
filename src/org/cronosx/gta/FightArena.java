package org.cronosx.gta;

import org.cronosx.gta.Fighting.Attack;
import org.cronosx.gta.Fighting.Defense;
import org.cronosx.gta.zones.ZoneArena;
import org.cronosx.gta.zones.ZoneArena.Challenge;
import org.cronosx.server.User;

public class FightArena extends Fight
{
	private float money;
	private int hp1;
	private int hp2;
	private Challenge c;
	
	private FightArena(Fighting player1, Fighting player2, float money)
	{
		super(player1, player2);
		hp1 = player1.getHP();
		hp2 = player2.getHP();
		this.money = money;
	}
	
	public FightArena(Challenge c, ZoneArena arena)
	{
		this(c.player1, c.player2, c.money);
		this.c = c;
		c.announceFightBegan(this, player1, player2);
		arena.removeChallenge(c);
	}
	
	public Fighting getChallenger()
	{
		return player1;
	}
	
	public Fighting getChallenged()
	{
		return player2;
	}

	@Override
	public void fightEnded(Fighting winner, Fighting loser)
	{
		((User)player1).doHP(hp1 - player1.getHP());
		((User)player2).doHP(hp2 - player2.getHP());
		winner.giveMoney(money*2);
		c.announceFightEnded(this, winner, loser);
	}
	
	@Override
	public void roundEnded(Fighting attacker, Attack attack, Defense defense, int hpDealed, int hpRevoked)
	{
	}
	
}
