package org.cronosx.gta;

import java.lang.reflect.Method;

import org.cronosx.server.User;


/**
 * Talk a the class for all lines of persons and users It specifies the text
 * one is actually saying and any number of possible answers as well as a
 * Method that can be called when anyone says this
 * 
 * @author ruebsteck
 * 
 */
public class Talk {

	private String text;
	private int nexts;
	private Method methode;
	private Person caller;
	private boolean isMultipleChoice;

	public Talk(String text, int nexts) {
		this.text = text;
		this.nexts = nexts;
		isMultipleChoice = false;
	}

	public Talk(String text, Method method, Person person, int nexts) {
		this.methode = method;
		this.caller = person;
		System.out.println("Method appointed: " + this.methode + " <- " + method);
		this.text = text;
		this.nexts = nexts;
		isMultipleChoice = false;
	}

	public Talk(String text, int nexts, boolean isMultipleChoice) {
		this.text = text;
		this.nexts = nexts;
		this.isMultipleChoice = isMultipleChoice;
	}

	public Talk(String text, Method method, int nexts, boolean isMultipleChoice) {
		this.methode = method;
		this.text = text;
		this.nexts = nexts;
		this.isMultipleChoice = isMultipleChoice;
	}
	
	public int getNexts() {
		return nexts;
	}

	public void setNexts(int nexts){
		this.nexts = nexts; 
	}
	
	public String getText() {
		return text;
	}

	public boolean isMultipleChoice() {
		return isMultipleChoice;
	}

	public void setMultipleChoice(boolean isMultipleChoice) {
		this.isMultipleChoice = isMultipleChoice;
	}

	public Method getMethod() {
		return methode;
	}

	
	public void setMethod(Method method) {
		this.methode = method;
	}

	public boolean noMoreAnswers() {
		return nexts == 0;
	}

	public void invoke(Person person, User user) {
		if (methode != null) {
			try {
				methode.invoke(person, user);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
//	public void invoke(String string){
//		if (method != null) {
//			try {
//				method.invoke(string);
//			}
//			catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}

	public String invoke(String string) {
		String out = "";
//		System.out.println("MEthod: " + this.methode);
		if (methode != null) {
			try {
				out = (String) methode.invoke(caller, string);
			}
			catch(ClassCastException e){
				out = null;//"Hier hat leider jemand einen Fehler gemacht :(";
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		else{
			out = null; //"Hier hat jemand vergessen einen alternativ Text anzugeben :(";
		}
		return out;
	}
}