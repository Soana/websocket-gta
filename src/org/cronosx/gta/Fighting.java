
package org.cronosx.gta;

import org.cronosx.server.Main;

/**
 * @author prior
 * represents someone or something that can be fighting
 */
public interface Fighting
{
	/**
	 * Called when it's this objects turn to attack
	 */
	public void attack();
	
	/**
	 * Called when it's this objects turn to defend
	 * @param attack incoming attack 
	 */
	public void defend(Attack attack);
	
	/**
	 * Should return in percent, how much this object is alive
	 * @return how much this object is alive (in percent)
	 */
	public float getAlive();
	
	
	/**
	 * @author prior
	 * Represents an attack, in however way
	 */
	abstract class Attack
	{
		/**
		 * @return a description from the view of the attacked 
		 */
		public abstract String getDescription();
		
		/**
		 * @return a name the attacker can select
		 */
		public abstract String getName();
		
		/**
		 * @return the amount of attack dealed
		 */
		public abstract int getATK();
		
		/**
		 * @return the probability of the attack to suceced as a floatingpoint value from 0 to 1
		 */
		public abstract float getPRB();
		
		/**
		 * @return the actual dealed attackpoints, after calculating the proximity
		 */
		public int execute()
		{
			if(Main.randomize(getPRB())) return getATK();
			else return 0;
		}
	}
	
	/**
	 * @author prior
	 * Represents a defense, in however way
	 */
	abstract class Defense
	{
		/**
		 * @return a name the defender can select
		 */
		public abstract String getName();
		/**
		 * @return a description from the view of the attacker (that one the defense is against)
		 */
		public abstract String getDescription();
		/**
		 * @return the amount of attackpoints to be negoitiatd
		 */
		public abstract int getDEF();
		/**
		 * @return the probability of the defense to suceced as a floatingpoint value from 0 to 1
		 */
		public abstract float getPRB();
		/**
		 * @return the actual negotiating defense-points, after calculating the proximity
		 */
		public int execute()
		{
			if(Main.randomize(getPRB())) return getDEF();
			else return 0;
		}
	}
	
	public String getName();
	
	/**
	 * Called after a round, should tell the users how much HP they (and their opponents) have left
	 * @param opponent
	 */
	public abstract void tellHP(Fighting opponent);
	
	/**
	 * Called when the defender selected it's defense-strategy 
	 * To display a message for example
	 * <p>
	 * @param def defense launched
	 * @param atk attack launched
	 * @param rest attack points remaining
	 */
	public void tellOutcome(Attack attack, Defense defense, int atk, int def, int rest);
	
	/**
	 * @return whether the object is still alive or whether not
	 */
	public boolean isDead();
	
	/**
	 * Called when the fight ended and this object has won 
	 */
	public void endFightWinning();
	
	/**
	 * Called when the fight has ended and this object has lost the fight 
	 */
	public void endFightLosing();
	

	/**
	 * Called when the fight has ended and this object escaped.
	 */
	public void endFightEscapedSelf();
	
	/**
	 * Called when the fight has ended because the enemy escaped.
	 */
	public void endFightEscapedEnemy();
	/**
	 * Has to return the amount of money that can be taken from this object after the fight has ended
	 * <p>
	 * @return the amount of money taken
	 */
	public float takeMoney();
	
	/**
	 * Called after the end of a fight
	 * Gives the object the amount of money taken from the object that has lost
	 * <p>
	 * @param i amount of money gained
	 */
	public void giveMoney(float i);
	
	/**
	 * Deals the attacked damage after a fight
	 * @param i damage
	 */
	public void dealDamage(int i);

	
	/**
	 * Should return the maximum healthpoints of the enemy (e.g. the healthpoints it has at the beginning of the battle)
	 * @return the healthpoints this enemy has at the beginning 
	 */
	public int getMaxHP();
	public int getHP();
	
}
