package org.cronosx.gta;

/**
 * @author prior
 * Represents an Enemy
 * (wow, who would have thought so!?)
 */
public abstract class Enemy implements Fighting
{
	protected Fight fight;
	protected int hp = getMaxHP();
	
	public Enemy()
	{
	}
	
	/**
	 * Must be set before the fight begins. Tells the enemy which fight to fight in
	 * @param fight fight to set
	 */
	public void setFight(Fight fight)
	{
		this.fight = fight;
	}
	
	/**
	 * Should return the maximum healthpoints of the enemy (e.g. the healthpoints it has at the beginning of the battle)
	 * @return the healthpoints this enemy has at the beginning 
	 */
	public int getHP()
	{
		return hp;
	}
	
	public float getAlive()
	{
		return getHP()/(float)getMaxHP();
	}
	
	public abstract String getName();
	public abstract String getDescription();
	protected abstract Attack[] getAttacks();
	protected abstract Defense[] getDefenses();
	
	protected abstract Attack pickAttack();
	protected abstract Defense pickDefense();
	
	protected Attack pickRandomAttack()
	{
		return getAttacks()[(int) (Math.random() * getAttacks().length)];
	}
	
	protected Defense pickRandomDefense()
	{
		return getDefenses()[(int) (Math.random() * getDefenses().length)];
	}
	
	
	public void inflictDamage(int dmg)
	{
		
	}

	@Override
	public void attack()
	{
		fight.incomingAttack(this, pickAttack());
	}

	@Override
	public void defend(Attack attack)
	{
		fight.incomingDefense(this, pickDefense());
	}

	@Override
	public void tellOutcome(Attack attack, Defense defense, int atk, int def, int rest)
	{
		
	}

	@Override
	public boolean isDead()
	{
		return getHP() <= 0;
	}

	@Override
	public void endFightWinning()
	{
		
	}

	@Override
	public void endFightLosing()
	{
		
	}

	@Override 
	public void endFightEscapedSelf()
	{
		
	}
	
	@Override
	public void endFightEscapedEnemy()
	{
	
	}
	
	@Override
	public void giveMoney(float i)
	{
		
	}

	@Override
	public void tellHP(Fighting opponent)
	{
		
	}
	
	@Override
	public void dealDamage(int i)
	{
		hp = hp -i <0 ? 0 : hp -i;
	}
	
}
