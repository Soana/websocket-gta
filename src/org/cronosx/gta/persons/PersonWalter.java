package org.cronosx.gta.persons;

import org.cronosx.gta.Person;
import org.cronosx.gta.Talk;


public class PersonWalter extends Person {

	/*public PersonWalter(){
		super();
		person.add( /* 0 *//* new Talk("Tach auch.\n", 1, 2, 3, 4));
		person.add( /* 1 *//* new Talk("Ganz gut, ich kann nicht klagen. Allerdings kriege ich von diesem kleinen Besen langsam echt Rückenschmerzen.\n", 
							2, 3, 4));
		person.add( /* 2 *//* new Talk("Du kennst mich nicht? Bist wohl neu in der Stadt. Ich bin Walter. Die meisten nennen mich den alten Walter.\n", 
							1, 3, 4));
		person.add( /* 3 *//* new Talk("Willst du, dass die überall in userer schönen Stadt rumliegen? Ich nicht und die Trottel in ihrem Rathaus kriegen es auch nicht auf die Reihe, dass hier mal jemand sauber macht. Also mach ich das.\n", 
							1, 2, 4));
		person.add( /* 4 *//* new Talk("Bis dann\n"));
		try
		{
			user.add( /* 0 *//* new Talk("Moin!\n", getClass().getMethod("addBierQuest", User.class), 0));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		user.add( /* 1 *//* new Talk("Wie geht's?", 1));
		user.add( /* 2 *//* new Talk("Wie heißt du eigentlich?", 2));
		user.add( /* 3 *//* new Talk("Warum fegst du die Blätter zusammen?", 3));
		user.add( /* 4 *//* new Talk("Ich will dann mal weiter...", 4));
	}
	
	public void addBierQuest(User user)
	{
		Quest q = new QuestBierWalter(user);
		if(!user.hasQuest(q.getTag())) user.addQuest(q);
	}
	
	@Override
	public int[] getStandardAnswers()
	{
		return new int[]{1,2,3,4};
	}*/
	
	public PersonWalter(){
		super();
		try {
			addTopLevelConversation("user", 
							new Talk("Moin", 1), 
							new Talk("Tach auch", 5, true), 
							new Talk("Wie geht's?", 2), 
							new Talk("Wer bist du eigentlich?", 1), 
							new Talk("Warum fegst du die Blätter zusammen?", 1),
							new Talk("suppress", getClass().getMethod("suppressed", String.class),this, 0),
							new Talk("QUEST bier", getClass().getMethod("addBierQuest", String.class),this, 1));//,
							//new Talk("Ich will dann man wieder...", 1));
			conversation.get("Main").setExitLine(new Talk("Ich will dann man wieder...", 1));
			
			addConversation( "user", 
							new Talk("Wie geht's?", 2),
							new Talk("Ganz gut, ich kann nicht klagen. Allerdings kriege ich von diesem kleinen Besen langsam echt Rückenschmerzen.\n" +
									"Und dir?", 1),
							new Talk("Beschissen...", 1));
			addConversation( "user", 
							new Talk("Wer bist du eigentlich?", 1),
							new Talk("Du kennst mich nicht? Bist wohl neu in der Stadt. Ich bin Walter. Die meisten nennen mich den alten Walter.", 0));
			addConversation( "user", 
							new Talk("Warum fegst du die Blätter zusammen?", 1), 
							new Talk("Willst du, dass die überall in userer schönen Stadt rumliegen? Ich nicht und die Trottel in ihrem Rathaus kriegen es auch nicht auf die Reihe, dass hier mal jemand sauber macht. Also mach ich das.", 1),
							new Talk("Nee, natürlich nicht", 1),
							new Talk("Das will ich dir auch geraten haben!", 0));
			addConversation( "user", 
							new Talk("Ich will dann man wieder...", 1),
							new Talk("Bis dann", 0));
			addConversation( "person", 
							new Talk("Ganz gut, ich kann nicht klagen. Allerdings kriege ich von diesem kleinen Besen langsam echt Rückenschmerzen.\n" +
									"Und dir?", 1),
							new Talk("Beschissen...", 0));
			addConversation( "person", 
							new Talk("Beschissen...", 1),
							new Talk("Wir haben doch aber so guten Wetter", 1),
							new Talk("Aber nicht doch", 0));
			addConversation( "user", 
					new Talk("QUEST bier", 1),
					new Talk("Beer for everyone", 1),
					new Talk("Juhee!!!", 0));
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String suppressed(String string){
		return "Ich bin dumm";
	}
	
	public String addBierQuest(String string){
		return "Ich will ein Bier";
		
	}
	
	@Override
	public String getName() {
		return "Walter";
	}

	@Override
	public String getDescription() {
		return "Den alten Walter kennt eigentlich jeder in der Stadt, auch wenn keinem wirklich klar ist, was genau er tut.\n" +
				"Vielleicht ist er bei der Straßenreinigung, vielleicht leistet er auch immer wieder Sozialstunden ab. Man munkelt ja so einiges...\n" +
				"Im Herbst fegt er in der Innenstadt das Laub zusammen.\n" +
				"Sonst trifft man ihn auf der Bank an, die dort steht oder in der Kneipe im Ghetto.\n" +
				"Er ist ein freundlicher alter Kauz, wenn man mit ihm redet, und er freut sich immer, wenn irgendjemand mit ihm Bier trinken geht.\n";
	}


	@Override
	public Talk getStandardExitLine() {
		return new Talk("Wenn du das sagst...", 0);
	}

	/*@Override
	public int[] getStandardAnswers() {
		// TODO Auto-generated method stub
		return null;
	}*/


}
