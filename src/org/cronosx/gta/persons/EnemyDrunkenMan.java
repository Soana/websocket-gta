package org.cronosx.gta.persons;

import org.cronosx.gta.Enemy;
import org.cronosx.server.Main;

public class EnemyDrunkenMan extends Enemy
{

	public EnemyDrunkenMan()
	{
		super();
	}

	@Override
	public float takeMoney()
	{
		return Main.randomInRange(5, 20);
	}

	@Override
	public int getMaxHP()
	{
		return 30;
	}

	@Override
	public String getName()
	{
		return "Betrunkener Mann";
	}

	@Override
	public String getDescription()
	{
		return "Dieser betrunkene Mann sieht irgendwie ziemlich bedrohlich aus, alleine schon, wenn man seinen Atem riecht. Puh!\n";
	}

	@Override
	protected Attack[] getAttacks()
	{
		return new Attack[] {
				new Attack()
				{
					@Override public String getDescription() { return "Der Säufer versucht, dir einen Barhocker über den Schädel zu ziehen\n";}
					@Override public String getName() { return "Barhocker über den Kopf ziehen"; }
					@Override public int getATK() { return 8; }
					@Override public float getPRB() { return 0.7F; }	
				},
				new Attack()
				{
					@Override public String getDescription() { return "Er wirft mit einer leeren Bierflasche nach dir\n";}
					@Override public String getName() { return "Bierflasche werfen"; }
					@Override public int getATK() { return 12; }
					@Override public float getPRB() { return 0.4F; }	
				},
				new Attack()
				{
					@Override public String getDescription() { return "Der Säufer hat eine Bierflasche an einem Tisch zersplittert und will dich mit ihr erstechen\n";}
					@Override public String getName() { return "Mit Bierflasche schlagen"; }
					@Override public int getATK() { return 9; }
					@Override public float getPRB() { return 0.6F; }	
				}
		};
	}

	@Override
	protected Defense[] getDefenses()
	{
		return new Defense[] {
				new Defense()
				{
					@Override public String getDescription() { return "Der Besoffene zeigt für seine Verhältnisse unglaubliche Reaktion und duckt sich weg\n";}
					@Override public String getName() { return "Wegducken"; }
					@Override public int getDEF() { return 30; }
					@Override public float getPRB() { return 0.3F; }	
				},
				new Defense()
				{
					@Override public String getDescription() { return "Der Besoffene wehrt die Attacke ab, indem er die traurigen Überreste eines Stuhles verwendet\n";}
					@Override public String getName() { return "Abwehren"; }
					@Override public int getDEF() { return 5; }
					@Override public float getPRB() { return 0.7F; }	
				}
		};
	}

	@Override
	protected Attack pickAttack()
	{
		return pickRandomAttack();
	}

	@Override
	protected Defense pickDefense()
	{
		return pickRandomDefense();
	}
	
}
