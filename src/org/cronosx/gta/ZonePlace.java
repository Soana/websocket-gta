package org.cronosx.gta;


import org.cronosx.server.User;
import org.cronosx.server.ZoneManager;

/**
 * @author prior
 * Represents a place that can be walked to but not driven to by car (buildings)
 * By default has the command "verlassen" to leave this zone and get back to the super-zone
 */
public abstract class ZonePlace extends Zone
{

	/**
	 * @param zm instance of the global zone-manager
	 */
	public ZonePlace(ZoneManager zm)
	{
		super(zm);
		try
		{
			registerCommand("Verlassen", "verlassen");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Will be invoked when a user called "verlassen"
	 * <p>
	 * Makes the user leave this zone and get back to the super zone
	 * <p>
	 * @param u user that left this zone
	 * @param string void
	 */
	public void verlassen(User u, String string)
	{
		u.send("Du hast das Gebäude verlassen.\n");
		u.setZone(this.getSuperZone());
	}
	
	@Override
	public boolean canDrive()
	{
		return false;
	}
	
}
