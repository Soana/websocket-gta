package org.cronosx.gta;

import java.util.ArrayList;

/**
 * Describes a conversation between a Person and a User by defining a linear
 * strand of conversation until there is a choice between multiple answers
 * 
 * @author ruebsteck
 * 
 */
public class Conversation {

	private ArrayList<Talk> lines;
	/** defines who started this strand */
	private boolean starter;
	private boolean hasExitLine = false;

	/**
	 * Makes a new Conversation without assigning lines to either the person or
	 * the user. Defines who starts the conversation, meaning who says the first
	 * line in this conversation It must be either "person" or "user"
	 * 
	 * @param starter
	 *            the participant who says the first line
	 * @throws IllegalArgumentException
	 *             if starter istn't "person" or "user"
	 */
	public Conversation(String starter) throws IllegalArgumentException {
		lines = new ArrayList<Talk>();
		if (starter.toLowerCase().equals("user")) {
			this.starter = true;
		}
		else if (starter.toLowerCase().equals("person")) {
			this.starter = false;
		}
		else {
			throw new IllegalArgumentException("Starter must be \"user\" or \"person\"");
		}
	}

	/**
	 * Makes a new Conversation with assigning lines to both participants. The
	 * user lines come first. To notice the change from user line to person.
	 * line, userAnswers gives the number of user lines. Defines who starts the
	 * conversation, meaning who says the first line in this conversation It
	 * must be either "person" or "user"
	 * 
	 * @param starter
	 *            the participant who says the first line
	 * @param talk
	 *            all lines for both user and person. The user lines go first
	 * @throws IllegalArgumentException
	 *             if starter istn't "person" or "user"
	 */
	public Conversation(String starter, Talk... talk) throws IllegalArgumentException {
		lines = new ArrayList<Talk>();
		if (starter.toLowerCase().equals("user")) {
			this.starter = true;
		}
		else if (starter.toLowerCase().equals("person")) {
			this.starter = false;
		}
		else {
			throw new IllegalArgumentException("Starter must be \"user\" or \"person\"");
		}
		for(Talk t: talk){
			lines.add(t);
		}
	}

	public Talk getLastLine(){
		if(hasExitLine){
			return lines.get(lines.size()-2);
		}
		else{
			return lines.get(lines.size()-1);
		}
	}
	
	public Talk getExitLine() {
		if(hasExitLine){
			return lines.get(lines.size()-1);
		}
		else{
			return null;
		}
	}

	public void setExitLine(Talk talk) {
		if(hasExitLine){
			lines.set(lines.size()-1, talk);
		}
		else{
			lines.add(talk);
		}
		hasExitLine = true;
	}

	public boolean hasExitLine(){
		return hasExitLine;
	}
	
	public boolean isStarter() {
		return starter;
	}

	/**
	 * Adds lines Keep the conversation flow in mind! don't forget to start an new conversation if a
	 * participant can decide
	 * 
	 * @param talk
	 *            lines to add
	 */
	public void addLine(Talk... talk) {
		Talk exitLine = lines.get(lines.size()-1);
		if(hasExitLine){	
			lines.remove(lines.size()-1);
		}
		for (Talk t : talk) {
			lines.add(t);
		}
		if(hasExitLine){
			lines.add(exitLine);
		}
	}

	/**
	 * Returns a specific line
	 * 
	 * @param index
	 *            index of the line to return
	 * @return line at index
	 */
	public Talk getLine(int index) {
		return lines.get(index);
	}

	public ArrayList<Talk> getLines(){
		return lines;
	}
	
	
	public String toString(){
		return lines.get(0).getText();
	}
}
