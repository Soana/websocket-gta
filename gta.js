function saveLogin(us, pw)
{
	setCookie("username",us, 7);
	setCookie("password",pw, 7);
};

$().ready(function()
{
	var us = getCookie("username");
	var pw = getCookie("password");
	$('#prompt').focus();
	$('#prompt').keyup(function(e)
	{
		if(e.which == 13)
		{
			var p = $('#prompt');
			var v = p.val();
			if(v.length>=15 && v.substring(0,15) == "login-speichern")
			{
				var vs = v.split(" ");
				if(vs.length >= 3)
				{
					saveLogin(vs[1], vs[2]);
					$('#console').append("Username und passwort gespeichert! Der Login erfolgt in den nächsten 7 Tagen automatisch!<br /> Löschen per \"login-vergessen\"<br \>");
				}
				else
				{
					$('#console').append("so geht das Nicht!<br \>login-speichern [username] [password]<br \>");
				}
			}
			else
			if(v.length >= 15 && v.substring(0,15) == "login-vergessen")
			{
				eraseCookie("username");
				eraseCookie("password");
				refreshPage();
			}
			else
			{
				getWebsocket().send(v);
				$('#console').append("  <span style=\"color: blue;\">></span> <span style=\"color: grey;\">"+v+"</span><br>");
				p.val("");
			}
		}
	});
});